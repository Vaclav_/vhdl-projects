----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/13/2013 05:21:23 PM
-- Design Name: 
-- Module Name: zh1_testbench - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

entity zh1_testbench is
end zh1_testbench;

architecture behavioral of zh1_testbench is

    signal a_test : std_logic;
    signal b_test : std_logic;
    signal s_test : std_logic;
    signal c_test : std_logic;

component zh1
    Port ( a : in STD_LOGIC;
           b : in STD_LOGIC;
           s : out STD_LOGIC;
           c : out STD_LOGIC);
end component zh1;

begin

    half_adder : zh1
    port map(
        a => a_test,
        b => b_test,
        s => s_test,
        c => c_test
    );
    

    gen_test : process
    begin
        a_test <=	'0',
					'1' after 50 ns,
					'0' after 100 ns,
					'1' after 150 ns;
                     
       b_test <=	'0',
					'1' after 100 ns;
             
        wait;
    end process gen_test;
    

end behavioral;
