----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.03.2013 18:42:21
-- Design Name: 
-- Module Name: counter8_tb - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity counter8_tb is
   --
end counter8_tb;

architecture behavioral of counter8_tb is

	constant width : integer := 4;
	
    signal clk, rst, en, load : STD_LOGIC;
    signal din, cnt : STD_LOGIC_VECTOR(width-1 downto 0);
    
    component counter
	    generic (
	    	width : integer := 8
    	);
    	Port ( clk : in STD_LOGIC;
    	       rst : in STD_LOGIC;
    	       en : in STD_LOGIC;
    	       load : in STD_LOGIC;
    	       din : in STD_LOGIC_VECTOR (width-1 downto 0);
    	       cnt : out STD_LOGIC_VECTOR (width-1 downto 0)
    	       );
    end component counter;

begin

    uut : counter
    generic map (	--generic asszociacios lista
	    width => width	--entity width => tesbench width
    )
    port map (  clk => clk,
                rst => rst, 
                en => en,
                load => load,
                din => din,
                cnt => cnt
     );

    proc_clk_gen : process
    begin
        clk <= '0'; wait for 10 ns;
        clk <= '1'; wait for 10 ns;    
    end process proc_clk_gen;
    
    proc_stimulus : process
    begin
        rst <= '1',
               '0' after 40 ns;
               
        en <= '1',
              '0' after 200 ns,
              '1' after 300 ns;
              
       load <= '0',
               '1' after 250 ns,
               '0' after 270 ns,
               '1' after 400 ns,
               '0' after 420 ns;
       din <= (others => '1');
    wait;
    end process proc_stimulus;
    
end behavioral;
