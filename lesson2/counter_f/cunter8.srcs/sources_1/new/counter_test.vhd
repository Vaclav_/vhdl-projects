----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/13/2013 07:07:01 PM
-- Design Name: 
-- Module Name: counter_test - structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter_test is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           load : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (7 downto 0);
           cnt : out STD_LOGIC_VECTOR (7 downto 0));
end counter_test;

architecture structural of counter_test is

component counter
	generic (	--generic lista
		width : integer := 8	--8 a default erteke
	);
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           load : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (width-1 downto 0);
           cnt : out STD_LOGIC_VECTOR (width-1 downto 0)
           );
end component counter;

component clk_div
   generic (
            input_freq   : integer := 100_000_000;   -- default value: 100 MHz
            desired_freq : integer := 100            -- default value: 100 Hz
           );
   port (
         clk_in  : in  std_logic;   -- clock input
         clk_out : out std_logic    -- clock output
        );
end component clk_div;

	signal clk_4 : std_logic;
	signal en_n : std_logic;	--az enable negaltja

begin

	en_n <= not en;

	inst_clk_div : clk_div
	generic map(
		input_freq => 100_000_000,	--100MHz
		desired_freq => 4
	)
	Port map(
		clk_in => clk,
		clk_out => clk_4
	);
	
	inst_counter : counter
	generic map(
		width => 8
	)
	Port map(
		clk => clk_4,
		rst => rst,
		en => en_n,
		load => load,
		din => din,
		cnt => cnt	
	);

end structural;
