----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Zsolt Milotai
-- 
-- Create Date:    04:24:27 11/12/2011 
-- Design Name:    Clock division
-- Module Name:    clk_div - behavioral 
-- Project Name:   Clock division with a modulo-N counter
-- Target Devices: Spartan-6 Family (XC6SLX16 on Digilent Nexys 3 board)
-- Tool versions:  ISE Design Suite 13.2
-- Description:    This module should be used to generate low frequencies
--                 (< 3 MHz) where the internal DCM is not applicable.
--                 The desired frequency value must be in range:
--                 1 ... (input_freq / 2) Hz.
--
--                 The implementation contains a modulo-N counter that divide
--                 the input clock frequency to a desired value.
--
--                 Be careful with the accuracy of the generated clock! When
--                 the input clock is not an integral multiple of the output
--                 clock, decreasing the difference between input clock frequency
--                 and the desired frequency results more error.
--                 Furthermore the duty cycle of the generated clock is not 50%
--                 in all cases.
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.clk_div_pkg.all;

entity clk_div is
   generic (
            input_freq   : integer := 100_000_000;   -- default value: 100 MHz
            desired_freq : integer := 100            -- default value: 100 Hz
           );
   port (
         clk_in  : in  std_logic;   -- clock input
         clk_out : out std_logic    -- clock output
        );
end clk_div;

architecture behavioral of clk_div is

   -- calculate the modulo-N counter upper limit (actually N)
   constant clk_div_upper_limit : integer := (input_freq / desired_freq) - 1;	--az orajel osztasi arany
   
   -- signal for counting
   signal clk_div : unsigned(log2(clk_div_upper_limit) - 1 downto 0) := (others => '0');	--log2(clk_div_upper_limit) -> hany bites legyen a szamlalo

begin

   -- connect the MSB of the counter to the output
   clk_out <= clk_div(clk_div'left);	--a clk_div legbaloldalabbi elemet kosd ki a kimenetre

   -- clock division with modulo-N counter
   clk_div_proc: process(clk_in)
   begin
      if (rising_edge(clk_in)) then
         if (clk_div = clk_div_upper_limit) then
            clk_div <= (others => '0');      -- reset the counter
         else
            clk_div <= clk_div + 1;          -- counting up
         end if;
      end if;
   end process clk_div_proc;

end behavioral;

