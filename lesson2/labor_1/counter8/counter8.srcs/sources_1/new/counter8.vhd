----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/12/2013 09:29:43 PM
-- Design Name: 
-- Module Name: counter8 - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------


entity counter8 is
    port (
        clk  : in  std_logic;
        rst  : in  std_logic;
        en   : in  std_logic;
        load : in  std_logic;
        din  : in  std_logic_vector(7 downto 0);
        cnt  : out std_logic_vector(7 downto 0)
    );
end counter8;


----------------------------------------------------------------------------------


architecture behavioral of counter8 is

    signal q_cnt : unsigned(7 downto 0);

begin

    cnt <= std_logic_vector(q_cnt);

    proc_counter : process(clk)
    begin
        if (rising_edge(clk)) then
            if (rst = '1') then
                q_cnt <= x"00";
            elsif (en = '1') then
                if (load = '1') then
                    q_cnt <= unsigned(din);
                else
                    q_cnt <= q_cnt + 1;
                end if;
            end if;
        end if;
    end process proc_counter;

end behavioral;