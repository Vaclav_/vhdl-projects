----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/12/2013 09:53:00 PM
-- Design Name: 
-- Module Name: counter8_tb - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------


entity counter8_tb is
end counter8_tb;


----------------------------------------------------------------------------------


architecture behavioral of counter8_tb is

    signal clk, rst, en, load : std_logic;
    signal din, cnt : std_logic_vector(7 downto 0);

    component counter8
        port (
            clk  : in  std_logic;
            rst  : in  std_logic;
            en   : in  std_logic;
            load : in  std_logic;
            din  : in  std_logic_vector(7 downto 0);
            cnt  : out std_logic_vector(7 downto 0)
        );
    end component counter8;

begin

    -- Instantiate the UUT
    uut : counter8
    port map (
        clk  => clk,
        rst  => rst,
        en   => en,
        load => load,
        din  => din,
        cnt  => cnt
    );

    -- Clock generator
    proc_clk_gen : process
    begin
        clk <= '0'; wait for 10 ns;
        clk <= '1'; wait for 10 ns;
    end process proc_clk_gen;

    -- Stimulus generation
    proc_stimulus : process
    begin
        rst <= '1',
               '0' after 40 ns;
        en <= '1',
              '0' after 200 ns,
              '1' after 300 ns;
        load <= '0',
                '1' after 250 ns,
                '0' after 270 ns,
                '1' after 400 ns,
                '0' after 420 ns;
        din <= x"42";
    wait;
    end process proc_stimulus;

end behavioral;