----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/27/2013 06:29:58 PM
-- Design Name: 
-- Module Name: led_display_ctrl - rtl
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity led_display_ctrl is
	generic (
		g_sys_clk_freq : integer := 100_000_000;
		g_refresh_rate : integer := 100
	);
    Port ( i_sys_clk : in STD_LOGIC;
           i_sys_rst : in STD_LOGIC;
           i_test : in STD_LOGIC;
           i_en : in STD_LOGIC;
           i_digit_0 : in STD_LOGIC_VECTOR (3 downto 0);
           i_digit_1 : in STD_LOGIC_VECTOR (3 downto 0);
           i_digit_2 : in STD_LOGIC_VECTOR (3 downto 0);
           i_digit_3 : in STD_LOGIC_VECTOR (3 downto 0);
           i_dp : in STD_LOGIC_VECTOR (3 downto 0);
           o_segment : out STD_LOGIC_VECTOR (7 downto 0);
           o_digit : out STD_LOGIC_VECTOR (3 downto 0));
           
           
	   component clk_div
		   generic (
			   input_freq   : integer := 100_000_000;      -- default value: 100 MHz
			   desired_freq : integer := 100               -- default value: 100 Hz
		   );
		   port (
			   clk_in  : in  std_logic;    -- clock input
			   clk_out : out std_logic     -- clock output
		   );
       end component clk_div;
       
end led_display_ctrl;

architecture rtl of led_display_ctrl is

	constant digit_num : integer := 4;
	constant clk_fsm_freq : integer := g_refresh_rate * digit_num;

	type t_fsm_state is ( d0, d1, d2, d3);

	signal state : t_fsm_state;
	signal next_state : t_fsm_state;
	
	signal clk_divided : std_logic;
	signal w_clk_fsm : std_logic;
	
	signal c_digit : std_logic_vector(3 downto 0);
	signal c_segment : std_logic_vector(7 downto 0);
	signal c_dp : std_logic;
	
	signal dec_in : std_logic_vector(3 downto 0);
	signal dec_out : std_logic_vector(6 downto 0);
	
begin

	inst_clk_div : clk_div
	generic map (
			input_freq => g_sys_clk_freq,
			desired_freq => clk_fsm_freq
	)
	port map (
		clk_in => i_sys_clk,
		clk_out => clk_divided
	);
	
	BUFG_inst : BUFG
	port map (
	   O => w_clk_fsm, -- 1-bit output: Clock buffer output
	   I => clk_divided  -- 1-bit input: Clock buffer input
	);

	--next state logic
	proc_next_state : process(state)
	begin
		case state is
			when d0 =>
				next_state <= d1;
			when d1 =>
				next_state <= d2;
			when d2 =>
				next_state <= d3;
			when d3 =>
				next_state <= d0;
		end case;
	end process proc_next_state;
	
	--state memory
	proc_state_memory : process()
	begin
		if(rising_edge(w_clk_fsm)) then
			if(i_sys_rst = '1') then
				state <= d0;
			else
				state <= next_state;
			end if;
		end if;
	end process proc_state_memory;

	--output logic
	proc_output_logic : process( state, i_dp, i_digit0, i_digit1, i_digit2, i_digit3)
	begin
		case state is
			when d0 =>
				c_digit <= "1110";
				dec_in <= i_digit_0;
				c_dp <= i_dp(0);
			when d1 =>
				c_digit <= "1101";
				dec_in <= i_digit_1;
				c_dp <= i_dp(1);
			when d2 =>
				c_digit <= "1011";
				dec_in <= i_digit_2;
				c_dp <= i_dp(2);
			when d3 =>
				c_digit <= "0111";
				dec_in <= i_digit_3;
				c_dp <= i_dp(3);
		end case;
	end process proc_output_logic;
	
	-- binary to 7-segment decoder
	with dec_in select	--decoder in
	   dec_out <= B"0000001" when X"0",
	              B"1001111" when X"1",
	              B"0010010" when X"2",
	              B"0000110" when X"3",
	              B"1001100" when X"4",
	              B"0100100" when X"5",
	              B"0100000" when X"6",
	              B"0001111" when X"7",
	              B"0000000" when X"8",
	              B"0000100" when X"9",
	              B"0001000" when X"A",
	              B"1100000" when X"B",
	              B"0110001" when X"C",
	              B"1000010" when X"D",
	              B"0110000" when X"E",
	              B"0111000" when X"F",
	              B"1111111" when others;

	--test MUX
	proc_test process(i_test, c_segment)
	begin
		if(i_test = '1') then
			o_segment <= X"00";
		else
			o_segment <= c_segment;
		end if;
	end process proc_test;

	--enable MUX
	proc_enable : process(i_en, c_digit)
	begin
		if(i_en = '1') then
			o_digit <= c_digit;
		else
			o_digit <= "1111";
		end if;
	end process proc_enable;
	
	--concatenate dp & segment
	c_segment <= c_dp & dec_out; --& osszefuzes operator

end rtl;
