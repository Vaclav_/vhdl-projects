----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 30.04.2013 20:20:31
-- Module Name: FIR_filter - Structural
-- Project Name: FIR_filter
-- Description: Ez a top entitas, az alulatereszto szuro
--	Interface:
--		i_signalX : 12 bites elojeles bemenet
--		i_clk : orajel bemenet
--		i_rst : reset bemenet
--		o_signalY : 12 bites elojeles kimenet
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity FIR_filter is
    Port (
    	i_signalX : in signed (11 downto 0);
		i_clk : in STD_LOGIC;
		i_rst : in STD_LOGIC;
		o_signalY : out signed (11 downto 0)
	);
end FIR_filter;

architecture Structural of FIR_filter is

	--a tapek szama ( = fokszam + 1 )
	constant TAP_NUMBER : integer := 51;

	--LUT letrehozasa a koefficienseknek
	subtype	t_coefficient is signed (15 downto 0);
	type t_lut is array ((TAP_NUMBER-1)/2 downto 0) of t_coefficient;
	constant c_coeff : t_lut := (
		x"FFC4", x"FF4E", x"FF5D", x"00F0",	x"037F",
		x"0471", x"01F6", x"FE52", x"FE08", x"0190",
		x"038D", x"0017", x"FBAC", x"FDB8", x"0477",
		x"0550", x"FCDA", x"F73C", x"FFBC", x"0C3C",
		x"0701", x"F0CD", x"EAEE", x"1133", x"4F5D", x"6E18"
	);
	
	component Register_12
	    Port (
	    	i_D : in signed (11 downto 0);
			i_clk : in STD_LOGIC;
			i_clr : in STD_LOGIC;
			o_Q : out signed (11 downto 0)
		);
	end component Register_12;
	
	component Add_12_12
	    Port (
	    	i_A : in signed (11 downto 0);
			i_B : in signed (11 downto 0);
			o_SUM : out signed (12 downto 0)
		);
	end component Add_12_12;
	
	component Mult_16x13
	    Port ( 
	   		i_A : in signed (15 downto 0);
			i_B : in signed (12 downto 0);
			o_result : out signed (28 downto 0)
	    );
	end component Mult_16x13;
	
	component Add_N_29
		generic (
			LENGTH_A : integer := 29
		);
	    Port (
	    	i_A : in signed (LENGTH_A-1 downto 0);
			i_B : in signed (28 downto 0);
			o_SUM : out signed (LENGTH_A-1 downto 0);
			o_OVF : out STD_LOGIC
		);
	end component Add_N_29;
	
	component Saturation
	    Port (
	    	i_number : in signed (16 downto 0);
			o_saturated : out signed (11 downto 0)
		);
	end component Saturation;
	
	--a regiszterek kimenetei, 51 db a bemeneten
	type t_signed12_array is array (integer range <>) of signed(11 downto 0);
	signal s_taps : t_signed12_array(TAP_NUMBER-1 downto 0);
	
	--a 12 bites osszeadok kiemenetei, 25 db
	type t_signed13_array is array (integer range <>) of signed(12 downto 0);
	signal s_sum13 : t_signed13_array((TAP_NUMBER-1)/2-1 downto 0);
	--a kozepso tap: itt nincs osszeado, hanem a kozepso regiszer kimenetet bovitjuk egy bittel
	signal s_sumLast : signed(12 downto 0);

	--a koefficienssel valo szorzas eredmenyei, 26 db ( = osszeadok szama + 1 )
	type t_signed29_array is array (integer range <>) of signed(28 downto 0);
	signal s_results : t_signed29_array( (TAP_NUMBER-1)/2 downto 0);
	
	--accumulate
	--30 bites eredmeny 2 db
	type t_signed30_array is array (integer range <>) of signed(29 downto 0);
	signal s_acc30 : t_signed30_array(1 downto 0);
	--31 bites 4 db
	type t_signed31_array is array (integer range <>) of signed(30 downto 0);
	signal s_acc31 : t_signed31_array(3 downto 0);
	--32 bites 8 db
	type t_signed32_array is array (integer range <>) of signed(31 downto 0);
	signal s_acc32 : t_signed32_array(7 downto 0);
	--33 bites 11 db
	type t_signed33_array is array (integer range <>) of signed(32 downto 0);
	signal s_acc33 : t_signed33_array(10 downto 0);

	--a mar korlatozott kiementi jel
	signal s_ySat : signed(11 downto 0);

begin

	--az elso bemeneti regiszter
	inst_registerFirst : Register_12
	port map (
		i_clk => i_clk,
		i_clr => i_rst,
		i_D => i_signalX,
		o_Q => s_taps(0)
	);

	--bemeneti regiszterek, az elsot es az utolsot nem szamitva
	gen_registers : for i in 1 to TAP_NUMBER-2 generate
	inst_registerN : Register_12
			port map (
				i_clk => i_clk,
				i_clr => i_rst,
				i_D => s_taps(i-1),
				o_Q => s_taps(i)
			);
	end generate gen_registers;

	--az utolso regiszter a bemeneten
	inst_registerLast : Register_12
	port map (
		i_clk => i_clk,
		i_clr => i_rst,
		i_D => s_taps(TAP_NUMBER-2),
		o_Q => s_taps(TAP_NUMBER-1)
	);

	--az osszeadok
	gen_add_12_12 : for i in 0 to (TAP_NUMBER-1)/2-1 generate
	inst_add_12_12 : Add_12_12
	Port map(
		i_A => s_taps(i),
		i_B => s_taps(TAP_NUMBER-1-i),
		o_SUM => s_sum13(i)
	);
	end generate gen_add_12_12;

	--a kozepso tap-en nincs osszeado, hanem 1 bittel bovitem az erteket
	s_sumLast <= s_taps((TAP_NUMBER-1)/2)(11) & s_taps((TAP_NUMBER-1)/2); 
	
	--szorzok, 25 db az utolsot nem szamitva
	gen_multipliers : for i in (TAP_NUMBER-1)/2-1 downto 0 generate
	inst_mult : Mult_16x13
	Port map(
		i_A => c_coeff((TAP_NUMBER-1)/2-i),
		i_B => s_sum13(i),
		o_result => s_results(i)
	);
	end generate gen_multipliers;
	
	--az utolso szorzo 
	inst_multLast : Mult_16x13
	port map(
	   	i_A => c_coeff(0),
		i_B => s_sumLast,
		o_result => s_results((TAP_NUMBER-1)/2)
	);
	
	--itt kezdodik az accumulate muvelet (osszeadok)
	-- N = 29; 1 db
	inst_add_29_29 : Add_N_29
	generic map(
		LENGTH_A => 29
	)
	port map(
		i_A => s_results(0),
		i_B => s_results(1),
		o_SUM => s_acc30(0)(28 downto 0),
		o_OVF => s_acc30(0)(29)
	);

	-- N = 30; 2 db
	inst_add_30_29_1 : Add_N_29
	generic map(
		LENGTH_A => 30
	)
	port map(
		i_A => s_acc30(0),
		i_B => s_results(2),
		o_SUM => s_acc30(1),
		o_OVF => open			--unconnected port
	);

	inst_add_30_29_2 : Add_N_29
	generic map(
		LENGTH_A => 30
	)
	port map(
		i_A => s_acc30(1),
		i_B => s_results(3),
		o_SUM => s_acc31(0)(29 downto 0),
		o_OVF => s_acc31(0)(30)
	);

	-- N = 31; 4 db
	gen_adders_31_29 : for i in 0 to 2 generate
		inst_add_31_29 : Add_N_29
		generic map(
			LENGTH_A => 31
		)
		port map(
			i_A => s_acc31(i),
			i_B => s_results(4+i),	--mert a s_results(4) az elso
			o_SUM => s_acc31(1+i),	--az 1-es az elso
			o_OVF => open
		);	
	end generate gen_adders_31_29;
	
	inst_add_31_29Last : Add_N_29
	generic map(
		LENGTH_A => 31
	)
	port map(
		i_A => s_acc31(3),
		i_B => s_results(7),
		o_SUM => s_acc32(0)(30 downto 0),
		o_OVF => s_acc32(0)(31)
	);
	
	-- N = 32; 8 db
	gen_adders_32_29 : for i in 0 to 6 generate
		inst_add_32_29 : Add_N_29
		generic map(
			LENGTH_A => 32
		)
		port map(
			i_A => s_acc32(i),
			i_B => s_results(8+i),	-- s_results(8) az elso
			o_SUM => s_acc32(1+i),	--az 1-es az elso
			o_OVF => open
		);	
	end generate gen_adders_32_29;
	
	inst_add_32_29Last : Add_N_29
	generic map(
		LENGTH_A => 32
	)
	port map(
		i_A => s_acc32(7),
		i_B => s_results(15),
		o_SUM => s_acc33(0)(31 downto 0),
		o_OVF => s_acc33(0)(32)
	);
	
	-- N = 33; 10 db
	gen_adders_33_29 : for i in 0 to 8 generate
		inst_add_33_29 : Add_N_29
		generic map(
			LENGTH_A => 33
		)
		port map(
			i_A => s_acc33(i),
			i_B => s_results(16+i),	-- s_results(16) az elso
			o_SUM => s_acc33(1+i),	--az 1-es az elso
			o_OVF => open
		);	
	end generate gen_adders_33_29;
	
	inst_add_33_29Last : Add_N_29
	generic map(
		LENGTH_A => 33
	)
	port map(
		i_A => s_acc33(9),
		i_B => s_results(25),
		o_SUM => s_acc33(10),
		o_OVF => open
	);

	--a kimenet korlatozasa
	inst_sat : Saturation
	port map(
		i_number => s_acc33(10)(32 downto 16),
		o_saturated => s_ySat
	);
	
	--kimeneti regiszter
	inst_registerOutput : Register_12
	port map (
		i_clk => i_clk,
		i_clr => i_rst,
		i_D => s_ySat,
		o_Q => o_signalY
	);

end Structural;
