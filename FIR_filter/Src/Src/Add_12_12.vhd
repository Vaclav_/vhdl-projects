----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 30.04.2013 19:21:35
-- Module Name: Add_12_12 - Behavioral
-- Project Name: FIR_filter
-- Description: 12 bites elojeles osszeado
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Add_12_12 is
    Port (
    	i_A : in signed (11 downto 0);
		i_B : in signed (11 downto 0);
		o_SUM : out signed (12 downto 0)
	);
end Add_12_12;

architecture Behavioral of Add_12_12 is

	--ext -> extended, azaz egy bittel kibovitett
	signal s_Aext : signed(12 downto 0);
	signal s_Bext : signed(12 downto 0);

begin

	s_Aext <= i_A(11) & i_A;
	s_Bext <= i_B(11) & i_B;

	o_SUM <= s_Aext + s_Bext;

end Behavioral;
