----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 30.04.2013 19:19:19
-- Module Name: Mult_16x13 - Behavioral
-- Project Name: FIR_filter
-- Description: 16x13 bites elojeles szorzo
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Mult_16x13 is
    Port ( 
   		i_A : in signed (15 downto 0);
		i_B : in signed (12 downto 0);
		o_result : out signed (28 downto 0)
    );
end Mult_16x13;

architecture Behavioral of Mult_16x13 is

begin

	o_result <= i_A * i_B;

end Behavioral;
