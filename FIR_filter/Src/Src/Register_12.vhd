----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 30.04.2013 17:09:16
-- Module Name: Register_12 - Behavioral
-- Project Name: FIR_filter
-- Description: 12 bites regiszter
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Register_12 is
    Port (
    	i_D : in signed (11 downto 0);
		i_clk : in STD_LOGIC;
		i_clr : in STD_LOGIC;
		o_Q : out signed (11 downto 0)
	);
end Register_12;

architecture Behavioral of Register_12 is

begin

	p_update : process( i_clk, i_clr )
	begin
		if( i_clr = '0' ) then
			if( rising_edge(i_clk) ) then
				o_Q <= i_D;
			end if;
		else
			o_Q <= x"000";
		end if;
	end process p_update;

end Behavioral;
