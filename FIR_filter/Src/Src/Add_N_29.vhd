----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 30.04.2013 19:27:01
-- Module Name: Add_N_29 - Behavioral
-- Project Name: FIR_filter
-- Description: N es 29 btes elojeles osszeado
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Add_N_29 is
	generic (
		LENGTH_A : integer := 29
	);
    Port (
    	i_A : in signed (LENGTH_A-1 downto 0);
		i_B : in signed (28 downto 0);
		o_SUM : out signed (LENGTH_A-1 downto 0);
		o_OVF : out STD_LOGIC
	);
end Add_N_29;

architecture Behavioral of Add_N_29 is

	--a kibovitett jelek
	signal s_Aext : signed (LENGTH_A downto 0);
	signal s_Bext : signed (LENGTH_A downto 0);
	signal s_SUMext : signed (LENGTH_A downto 0);

begin

	--bovites 1 bittel
	s_Aext <= i_A(LENGTH_A-1) & i_A;

	p_extendB : process(i_B)
	begin
		--i_B bovitese LENGTH_A+1 hosszusagura
		for i in 29 to LENGTH_A loop
			s_Bext(i) <= i_B(28);
		end loop;
			s_Bext(28 downto 0) <= i_B;
	end process p_extendB;

	--osszeadas az azonos hosszu buszokkal
	s_SUMext <= s_Aext + s_Bext;
	
	--kimenetek
	o_SUM <= s_SUMext(LENGTH_A-1 downto 0);
	o_OVF <= s_SUMext(LENGTH_A);

end Behavioral;
