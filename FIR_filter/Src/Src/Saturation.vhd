----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 03.05.2013 23:15:32
-- Module Name: Saturation - Behavioral
-- Project Name: FIR_filter
-- Description: Korlatozas 12 bitre
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Saturation is
    Port (
    	i_number : in signed (16 downto 0);
		o_saturated : out signed (11 downto 0)
	);
end Saturation;

architecture Behavioral of Saturation is

begin

	p_output : process(i_number)
	begin
		--max
		if( i_number > 2047 ) then
			o_saturated <= x"7ff";
		--min
		elsif( i_number < -2048 ) then
			o_saturated <= x"800";
		else --kulonben nincs korlatozas
			o_saturated <= i_number(11 downto 0);
		end if;
	end process p_output;

end Behavioral;
