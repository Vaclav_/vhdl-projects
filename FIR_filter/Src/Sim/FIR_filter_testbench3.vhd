----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 04.05.2013 13:27:02
-- Module Name: FIR_filter_testbench3 - Behavioral
-- Project Name: FIR_filter
-- Description: Szimulacio:
--	Szimulacios ido: 2000 ns
--	Orajel: 10 ns
--	Gerjesztes: impulzus
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity FIR_filter_testbench3 is
end FIR_filter_testbench3;

architecture Behavioral of FIR_filter_testbench3 is

	component FIR_filter
		Port (
			i_signalX : in signed (11 downto 0);
			i_clk : in STD_LOGIC;
			i_rst : in STD_LOGIC;
			o_signalY : out signed (11 downto 0)
		);
	end component FIR_filter;
	
	component logFile is
		generic (
	         fileName : string := "log.txt"
		);
	    Port (
	    	i_clk : in STD_LOGIC;
	        i_y1 : in signed(11 downto 0);
	        i_y2 : in signed(11 downto 0)
	    );
	end component logFile;

	signal t_signalX : signed (11 downto 0);
	signal t_clk : STD_LOGIC;
	signal t_rst : STD_LOGIC;
	signal t_signalY : signed (11 downto 0);

begin

	uut : FIR_filter
	port map(
		i_signalX => t_signalX,
		i_clk => t_clk,
		i_rst => t_rst,
		o_signalY => t_signalY
	);
	
	inst_logFile : logFile
	generic map(
		fileName => "log3.txt"
	)
	port map(
		i_clk => t_clk,
	    i_y1 => t_signalX,
	    i_y2 => t_signalY
	);

	p_genClk : process
	begin
		t_clk <= '0';
		wait for 5 ns;
		t_clk <= '1';
		wait for 5 ns;
	end process p_genClk;

	p_stim : process
	begin
		t_rst <= '0';

		t_signalX <= 	x"000",
						x"7ff" after 802 ns,
						x"000" after 812 ns;
		
		wait;
	end process p_stim;

end Behavioral;
