----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 30.04.2013 17:21:07
-- Module Name: Register_12_testbench - Behavioral
-- Project Name: FIR_filter
-- Description:
--	A Register_12 entitas tesztelese.
--	Orajel: 50 ns
--	Beolvasas tesztelese: 0-500 ns
--	Torles funkcio tesztelese: 500-1000 ns
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Register_12_testbench is
end Register_12_testbench;

architecture Behavioral of Register_12_testbench is
	component Register_12
		Port (
			i_D : in signed (11 downto 0);
			i_clk : in STD_LOGIC;
			i_clr : in STD_LOGIC;
			o_Q : out signed (11 downto 0)
		);
	end component Register_12;
	
	signal t_D : signed (11 downto 0);
	signal t_clk : STD_LOGIC;
	signal t_clr : STD_LOGIC;
	signal t_Q : signed (11 downto 0);
begin

	uut : Register_12
	port map(
		i_D => t_D,
		i_clk => t_clk,
		i_clr => t_clr,
		o_Q => t_Q
	);

	p_clock : process
	begin
		t_clk <= '0';
		wait for 25 ns;
		t_clk <= '1';
		wait for 25 ns;
	end process p_clock;

	p_stim : process
	begin
		t_D <=		x"003",
					x"14a" after 60 ns,
					x"100" after 220 ns,
					x"fff" after 360 ns,
					x"001" after 600 ns,
					x"010" after 800 ns;

		t_clr <=	'0',
					'1' after 520 ns,
					'0' after 850 ns;
		wait;
	end process p_stim;
end Behavioral;
