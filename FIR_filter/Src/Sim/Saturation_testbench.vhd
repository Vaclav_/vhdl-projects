----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 04.05.2013 08:23:27
-- Module Name: Saturation_testbench - Behavioral
-- Project Name: FIR_filter
-- Description: 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Saturation_testbench is
end Saturation_testbench;

architecture Behavioral of Saturation_testbench is

	component Saturation
		Port (
			i_number : in signed (16 downto 0);
			o_saturated : out signed (11 downto 0)
		);
	end component Saturation;

	signal t_number : signed(16 downto 0);
	signal t_sat : signed(11 downto 0);

begin

	uut : Saturation
	port map(
		i_number => t_number,
		o_saturated => t_sat
	);

	p_stim : process
	begin
		t_number <= 	'0' & x"0002",
						'0' & x"1000" after 200 ns,
						'0' & x"7fff" after 400 ns,
						'1' & x"ff00" after 600 ns,
						'1' & x"2fff" after 800 ns;
		wait;
	end process p_stim;

end Behavioral;
