----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 30.04.2013 19:38:43
-- Module Name: Add_N_29_testbench - Behavioral
-- Project Name: FIR_filter
-- Description: A szimulacio: 
--	0-600 ns t_A pozitiv szam
--	600-1000 ns t_A negativ szam, tulcsodrulas
--	t_ elotag: a tesbench jelei
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Add_N_29_testbench is
end Add_N_29_testbench;

architecture Behavioral of Add_N_29_testbench is

	constant c_LENGTH_A : integer := 32;

	component Add_N_29
		generic (
			LENGTH_A : integer := 29
		);
		Port (
			i_A : in signed (LENGTH_A-1 downto 0);
			i_B : in signed (28 downto 0);
			o_SUM : out signed (LENGTH_A-1 downto 0);
			o_OVF : out STD_LOGIC
		);
	end component Add_N_29;

	signal t_A : signed (c_LENGTH_A-1 downto 0);
	signal t_B : signed (28 downto 0);
	signal t_SUM : signed (c_LENGTH_A-1 downto 0);
	signal t_OVF : STD_LOGIC;

begin

	uut : Add_N_29
	generic map (
	    LENGTH_A => c_LENGTH_A
	)
	port map(
		i_A => t_A,
		i_B => t_B,
		o_SUM => t_SUM,
		o_OVF => t_OVF
	);

	p_stim : process
	begin
		t_A <=	x"00000000",
				x"000000ff" after 200 ns,
				x"7fffffff" after 400 ns,
				x"80000000" after 800 ns;

		t_B <=	b"0" & x"0000001",
				b"1" & x"0000000" after 600 ns;
		wait;
	end process p_stim;

end Behavioral;
