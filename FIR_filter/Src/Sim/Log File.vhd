----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 03.05.2013 22:42:28
-- Module Name: Log File - Behavioral
-- Project Name: FIR_filter
-- Description:
--		A ket bemeneti jelet menti el egy fileba
--		A file szerkezete: 1. oszlop x tengely, 2. oszlop i_y1, 3. oszlop i_y2
----------------------------------------------------------------------------------

library ieee;
use ieee.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use std.textio.all;
use work.txt_util.all;

entity logFile is
	generic (
         fileName : string := "log.txt"
	);
    Port (
    	i_clk : in STD_LOGIC;
        i_y1 : in signed(11 downto 0);
        i_y2 : in signed(11 downto 0)
    );
end logFile;

architecture Behavioral of logFile is

	constant c_tab : character := ht;  
    file f_logs : TEXT open write_mode is fileName;
    signal s_x : integer := 0;
   
begin

p_write : process
begin
	print( f_logs, "#__x__y1__y2__#");
	wait until i_clk = '1';

	while true loop
		s_x <= s_x+10;
		print( f_logs, str(s_x) & c_tab & str(to_integer(i_y1)) & c_tab & str(to_integer(i_y2)) );
		wait until i_clk = '1';
	end loop;
end process p_write;

end Behavioral;
