----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/13/2013 07:07:01 PM
-- Design Name: 
-- Module Name: counter_test - structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;		--az FPGA primitivek hasznalatahoz kell

entity counter_test is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           load : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (7 downto 0);
           cnt : out STD_LOGIC_VECTOR (7 downto 0));
end counter_test;

architecture structural of counter_test is

component counter
	generic (	--generic lista
		width : integer := 8	--8 a default erteke
	);
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           load : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (width-1 downto 0);
           cnt : out STD_LOGIC_VECTOR (width-1 downto 0)
           );
end component counter;

component clk_div
   generic (
            input_freq   : integer := 100_000_000;   -- default value: 100 MHz
            desired_freq : integer := 100            -- default value: 100 Hz
           );
   port (
         clk_in  : in  std_logic;   -- clock input
         clk_out : out std_logic    -- clock output
        );
end component clk_div;

	signal clk_10 : std_logic;
	signal en_n : std_logic;	--az enable negaltja
	signal clk_divided : std_logic;
	signal control0 : std_logic_vector(35 downto 0);
	signal control1 : std_logic_vector(35 downto 0);
	signal c_rst : std_logic;
	signal c_en : std_logic;
	signal c_load : std_logic;
	signal v_cnt : std_logic_vector(7 downto 0);
	signal vio_out : std_logic_vector(2 downto 0);
	
	component icon
	  PORT (
	    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
	    CONTROL1 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
	
	end component;
	
	component ila
	  PORT (
	    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
	    CLK : IN STD_LOGIC;
	    TRIG0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0));
	
	end component;
	
	component vio
	  PORT (
	    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
	    CLK : IN STD_LOGIC;
	    SYNC_IN : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	    SYNC_OUT : OUT STD_LOGIC_VECTOR(2 DOWNTO 0));
	
	end component;

begin

inst_icon : icon
  port map (
    CONTROL0 => CONTROL0,
    CONTROL1 => CONTROL1);

inst_ila : ila
  port map (
    CONTROL => CONTROL0,
    CLK => CLK_10,
    TRIG0 => v_cnt);

inst_vio : vio
  port map (
    CONTROL => CONTROL1,
    CLK => CLK_10,
    SYNC_IN => v_cnt,
    SYNC_OUT => vio_out);

	BUFG_inst : BUFG
	port map (
	   O => clk_10, -- 1-bit output: Clock buffer output
	   I => clk_divided  -- 1-bit input: Clock buffer input
	);

	en_n <= not en;

	inst_clk_div : clk_div
	generic map(
		input_freq => 100_000_000,	--100MHz
		desired_freq => 10
	)
	Port map(
		clk_in => clk,
		clk_out => clk_divided
	);
	
	inst_counter : counter
	generic map(
		width => 8
	)
	Port map(
		clk => clk_10,
		rst => c_rst,
		en => c_en,
		load => c_load,
		din => din,
		cnt => v_cnt	
	);
	
	c_rst <= rst or vio_out(0);
	c_en <= en or vio_out(1);
	c_load <= load or vio_out(2);

end structural;
