----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.03.2013 17:53:56
-- Design Name: 
-- Module Name: counter8 - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity counter is
	generic (	--generic lista
		width : integer := 8	--8 a default erteke
	);
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           load : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (width-1 downto 0);
           cnt : out STD_LOGIC_VECTOR (width-1 downto 0)
           );
end counter;

architecture behavioral of counter is
    
    signal q_cnt : unsigned(width-1 downto 0) := (others => '0');
    
begin

    cnt <= STD_LOGIC_VECTOR(q_cnt);

    proc_counter : process( clk )

    begin
        if( rising_edge(clk) ) then
            if( rst = '1' ) then
                q_cnt <= (others => '0');	-- az osszes bitje legyen '0', VHDL aggregacio
            elsif( en = '1' ) then
                if( load = '1' ) then
                    q_cnt <= unsigned(din);
                else
                    q_cnt <= q_cnt + 1;
                end if;
            end if;        
        end if;
    end process proc_counter;
    
end behavioral;
