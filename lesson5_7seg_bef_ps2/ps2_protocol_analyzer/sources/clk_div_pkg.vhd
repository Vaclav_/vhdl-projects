----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Zsolt Milotai
-- 
-- Create Date:    04:20:36 11/12/2011 
-- Design Name:    Clock division package
-- Project Name:   Clock division with a modulo-N counter
-- Target Devices: Spartan-6 Family (XC6SLX16 on Digilent Nexys 3 board)
-- Tool versions:  ISE Design Suite 13.2
--	Purpose:        This package defines supplemental types, subtypes, 
--		             constants, and functions.
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package clk_div_pkg is

   -- function declaration
   function log2 (i : integer) return integer;

end clk_div_pkg;

package body clk_div_pkg is

   -- function definition
   
   -- calculate the logarithm base 2 of an integer
   function log2 (i : integer) return integer is
      variable n : integer := 0;
   begin
      while (2**n - 1) < i loop
         n := n + 1;
      end loop;
      return n;
   end log2;

end clk_div_pkg;
