----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    03:05:06 11/23/2011 
-- Design Name: 
-- Module Name:    ps2_protocol_analyzer - structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity ps2_protocol_analyzer is
   port (
         -- clock input
         clk                : in    std_logic;
         
         -- keyboard
         ps2_keyboard_clock : inout std_logic;
         ps2_keyboard_data  : inout std_logic;
         
         -- mouse
         ps2_mouse_clock    : inout std_logic;
         ps2_mouse_data     : inout std_logic
        );
end ps2_protocol_analyzer;

architecture structural of ps2_protocol_analyzer is

   -- Constants
   
   -- frequency of the system clock in Hz
   constant sys_clk_freq : integer := 100_000_000;
   constant ila_clk_freq : integer := 400_000;

   -- Signals

   -- ChipScope Pro signals
   signal control : std_logic_vector(35 downto 0);
   signal clk_ila : std_logic;
   signal keyboard : std_logic_vector(1 downto 0);
   signal mouse    : std_logic_vector(1 downto 0);

   -- Component declarations
   
   -- clock division
   component clk_div
      generic (
               input_freq   : integer;
               desired_freq : integer
              );
      port (
            clk_in  : in  std_logic;
            clk_out : out std_logic
           );
   end component;

   -- ChipScope Pro ICON
   component icon
      port (
            control0 : inout std_logic_vector(35 downto 0)
           );
   end component;

   -- ChipScope Pro ILA
   component ila
      port (
            control : inout std_logic_vector(35 downto 0);
            clk     : in    std_logic;
            trig0   : in    std_logic_vector(1 downto 0);
            trig1   : in    std_logic_vector(1 downto 0)
           );
   end component;

begin

   -- instantiate clk_div to generate desired clock for the ILA
   clk_div_inst: clk_div
   generic map (
                input_freq   => sys_clk_freq,
                desired_freq => ila_clk_freq
               )
   port map (
             clk_in  => clk,
             clk_out => clk_ila
            );

   -- instantiate ChipScope Pro ICON
   icon_inst: icon
      port map (
                control0 => control
               );

   -- instantiate ChipScope Pro ILA
   ila_inst: ila
      port map (
                control => control,
                clk     => clk_ila,
                trig0   => keyboard,
                trig1   => mouse
               );

   -- connect keyboard & mose signals to the ChipScope Pro ILA inputs
   keyboard <= ps2_keyboard_clock & ps2_keyboard_data;
   mouse <= ps2_mouse_clock & ps2_mouse_data;

end structural;

