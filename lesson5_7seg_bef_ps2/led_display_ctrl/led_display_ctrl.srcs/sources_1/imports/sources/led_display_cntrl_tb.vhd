----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Zsolt Milotai
--
-- Create Date:    23:28:31 11/12/2011
-- Design Name:    4-digit 7-segment display controller test bench
-- Module Name:    led_display_cntrl_tb - behavioral
-- Project Name:   4-digit 7-segment display controller
-- Target Devices: Spartan-6 Family (XC6SLX16 on Digilent Nexys 3 board)
-- Tool versions:  ISE Design Suite 13.2
-- Description:    This is a test bench for 4-digit 7-segment display
--                 controller module.
-- 
-- VHDL Test Bench Created by ISE for module: led_display_cntrl
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity led_display_cntrl_tb is
end led_display_cntrl_tb;

architecture behavioral of led_display_cntrl_tb is 

    -- Component Declaration for the Unit Under Test (UUT)

   component led_display_cntrl
      port (
            clk     : in  std_logic;
            rst     : in  std_logic;
            en      : in  std_logic;
            test    : in  std_logic;
            digit_0 : in  std_logic_vector(3 downto 0);
            digit_1 : in  std_logic_vector(3 downto 0);
            digit_2 : in  std_logic_vector(3 downto 0);
            digit_3 : in  std_logic_vector(3 downto 0);
            dp      : in  std_logic_vector(3 downto 0);
            segment : out std_logic_vector(7 downto 0);
            digit   : out std_logic_vector(3 downto 0)
           );
   end component;

   --Inputs
   signal clk     : std_logic := '0';
   signal rst     : std_logic := '0';
   signal en      : std_logic := '0';
   signal test    : std_logic := '0';
   signal digit_0 : std_logic_vector(3 downto 0) := (others => '0');
   signal digit_1 : std_logic_vector(3 downto 0) := (others => '0');
   signal digit_2 : std_logic_vector(3 downto 0) := (others => '0');
   signal digit_3 : std_logic_vector(3 downto 0) := (others => '0');
   signal dp      : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal segment : std_logic_vector(7 downto 0);
   signal digit   : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;

begin

	-- Instantiate the Unit Under Test (UUT)
   uut: led_display_cntrl
   port map (
             clk     => clk,
             rst     => rst,
             en      => en,
             test    => test,
             digit_0 => digit_0,
             digit_1 => digit_1,
             digit_2 => digit_2,
             digit_3 => digit_3,
             dp      => dp,
             segment => segment,
             digit   => digit
            );

   -- Clock process definitions
   clk_process: process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
      rst <= '1',
             '0' after 40 ns,
             '1' after 85 ms,
             '0' after 95 ms;
      en <= '0',
            '1' after 200 ns,
            '0' after 60 ms,
            '1' after 80 ms;
      test <= '0',
              '1' after 30 ms,
              '0' after 50 ms;
      dp <= X"2";
      digit_3 <= X"8";
      digit_2 <= X"C";
      digit_1 <= X"4";
      digit_0 <= X"3";
      wait;
   end process;

end behavioral;
