----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Zsolt Milotai
-- 
-- Create Date:    21:25:32 10/31/2011
-- Design Name:    4-digit 7-segment display controller
-- Module Name:    led_display_cntrl - behavioral
-- Project Name:   4-digit 7-segment display controller
-- Target Devices: Spartan-6 Family (XC6SLX16 on Digilent Nexys 3 board)
-- Tool versions:  ISE Design Suite 13.2
-- Description:    This is a controller for 4-digit 7-segment common anode
--                 display on Nexys 3 board.
--
--                 Features: - multiplexing digits
--                           - internal clock generation for multiplexing
--                           - adjustable display refresh rate
--                           - display enable / disable
--                           - display test (turn on all digits' segments)
--
--                 All digits are common anode ('0' cause a segment to turn on)
--                 and must be drived by logic '0' to enable.
--
--                 Segments:                       Digits:
--                              a
--                            -----                  ---    ---    ---    ---
--                         f |  g  | b              |   |  |   |  |   |  |   |
--                            -----                  ---    ---    ---    ---
--                         e |     | c              |   |  |   |  |   |  |   |
--                            -----  .               --- .  --- .  --- .  --- .
--                              d    dp               3.     2.     1.     0.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity led_display_cntrl is
   port (
         -- clock & reset
         clk      : in   std_logic;
         rst      : in   std_logic;
         
         -- control signals
         en       : in   std_logic;    -- display enable
         test     : in   std_logic;    -- display test
         
         -- data in / out
         digit_0  : in   std_logic_vector(3 downto 0);   -- 0. digit
         digit_1  : in   std_logic_vector(3 downto 0);   -- 1. digit
         digit_2  : in   std_logic_vector(3 downto 0);   -- 2. digit
         digit_3  : in   std_logic_vector(3 downto 0);   -- 3. digit
         dp       : in   std_logic_vector(3 downto 0);   -- decimal points
         
         -- 7-segment display
         segment  : out std_logic_vector(7 downto 0);    -- output to the segments
         digit    : out std_logic_vector(3 downto 0)     -- output to the digits
        );
end led_display_cntrl;

architecture behavioral of led_display_cntrl is

   -- Constants
   
   -- frequency of the system clock in Hz
   constant sys_clk_freq         : integer := 100_000_000;
   
   -- number of digits in the display
   constant display_digit_num    : integer := 4;

   -- the desired display refresh rate in Hz
   constant display_refresh_rate : integer := 100;

   -- multiplex digits with this frequency
   constant display_digit_clk    : integer := display_digit_num * display_refresh_rate;

   -- Type declarations
   
   -- FSM (Finite-State Machine) states
   type fsm_state_type is (d0, d1, d2, d3);

   -- Signals

   -- internal clock signal to multiplex digits
   signal clk_digit : std_logic;

   -- binary to 7-segment decoder signals
   -- decoded segment positions: a, b, c, d, e, f, g
   signal dec_in  : std_logic_vector(3 downto 0);
   signal dec_out : std_logic_vector(6 downto 0);
   
   -- FSM (Finite-State Machine) state signals 
   signal state      : fsm_state_type;
   signal next_state : fsm_state_type;
   
   -- internal segment & digit & decimal point signals
   signal segment_int : std_logic_vector(7 downto 0);
   signal digit_int   : std_logic_vector(3 downto 0);
   signal dp_int      : std_logic;

   -- Component declarations
   
   -- clock division
   component clk_div
      generic (
               input_freq   : integer;
               desired_freq : integer
              );
      port (
            clk_in  : in  std_logic;
            clk_out : out std_logic
           );
   end component;

begin

   -- instantiate clk_div to generate desired clock for the display
   clk_div_inst: clk_div
   generic map (
                input_freq   => sys_clk_freq,
                desired_freq => display_digit_clk
               )
   port map (
             clk_in  => clk,
             clk_out => clk_digit
            );

   -- binary to 7-segment decoder
   with dec_in select
      dec_out <= B"0000001" when X"0",
                 B"1001111" when X"1",
                 B"0010010" when X"2",
                 B"0000110" when X"3",
                 B"1001100" when X"4",
                 B"0100100" when X"5",
                 B"0100000" when X"6",
                 B"0001111" when X"7",
                 B"0000000" when X"8",
                 B"0000100" when X"9",
                 B"0001000" when X"A",
                 B"1100000" when X"B",
                 B"0110001" when X"C",
                 B"1000010" when X"D",
                 B"0110000" when X"E",
                 B"0111000" when X"F",
                 B"1111111" when others;

   -- concatenate decoder output & decimal point
   segment_int <= dec_out & (not dp_int);

   -- FSM state memory
   state_proc: process(clk_digit)
   begin
      if (rising_edge(clk_digit)) then
         if (rst = '1') then
            state <= d0;
         else
            state <= next_state;
         end if;
      end if;
   end process state_proc;

   -- FSM next-state logic
   next_state_proc: process(state)
   begin
      case state is
         when d0 =>
            next_state <= d1;
         when d1 =>
            next_state <= d2;
         when d2 =>
            next_state <= d3;
         when d3 =>
            next_state <= d0;
      end case;
   end process next_state_proc;

   -- FSM output logic
   out_proc: process(state, digit_0, digit_1, digit_2, digit_3, dp)
   begin
      case state is
         when d0 =>
            dec_in <= digit_0;
            dp_int <= dp(0);
            digit_int <= B"1110";   -- turn on 0. digit
         when d1 =>
            dec_in <= digit_1;
            dp_int <= dp(1);
            digit_int <= B"1101";   -- turn on 1. digit
         when d2 =>
            dec_in <= digit_2;
            dp_int <= dp(2);
            digit_int <= B"1011";   -- turn on 2. digit
         when d3 =>
            dec_in <= digit_3;
            dp_int <= dp(3);
            digit_int <= B"0111";   -- turn on 3. digit
      end case;
   end process out_proc;
   
   -- display enable mux
   en_proc: process(en, rst, digit_int)
   begin
      if (en = '1' and rst = '0') then
         digit <= digit_int;
      else
         digit <= B"1111";    -- disable all digits
      end if;
   end process en_proc;
   
   -- display test mux
   test_proc: process(test, segment_int)
   begin
      if (test = '1') then
         segment <= X"00";    -- turn on all segments
      else
         segment <= segment_int;
      end if;
   end process test_proc;

end behavioral;

