----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Zsolt Milotai
-- 
-- Create Date:    02:49:02 11/01/2011
-- Design Name:    4-digit 7-segment display controller test
-- Module Name:    led_display_cntrl_test - structural
-- Project Name:   4-digit 7-segment display controller
-- Target Devices: Spartan-6 Family (XC6SLX16 on Digilent Nexys 3 board)
-- Tool versions:  ISE Design Suite 13.2
-- Description:    This is a test for 4-digit 7-segment display controller. The
--                 controller inputs can be stimulated by ChipScope Pro VIO
--                 component and the output connected directly to the display.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity led_display_cntrl_test is
    port (
          sys_clk : in  std_logic;
          segment : out std_logic_vector(7 downto 0);
          digit   : out std_logic_vector(3 downto 0)
         );
end led_display_cntrl_test;

architecture structural of led_display_cntrl_test is

   -- Signals

   -- 4-digit 7-segment controller signals
   signal rst     : std_logic;
   signal en      : std_logic;
   signal test    : std_logic;
   signal digit_0 : std_logic_vector(3 downto 0);
   signal digit_1 : std_logic_vector(3 downto 0);
   signal digit_2 : std_logic_vector(3 downto 0);
   signal digit_3 : std_logic_vector(3 downto 0);
   signal dp      : std_logic_vector(3 downto 0);

   -- ChipScope Pro signals
   signal control : std_logic_vector(35 downto 0);
   signal vio_out : std_logic_vector(22 downto 0);

   -- Component declarations

   -- 4-digit 7-segment controller
   component led_display_cntrl
      port (
            rst      : in   std_logic;
            clk      : in   std_logic;
            en       : in   std_logic;
            test     : in   std_logic;
            digit_0  : in   std_logic_vector(3 downto 0);   -- 0. digit
            digit_1  : in   std_logic_vector(3 downto 0);   -- 1. digit
            digit_2  : in   std_logic_vector(3 downto 0);   -- 2. digit
            digit_3  : in   std_logic_vector(3 downto 0);   -- 3. digit
            dp       : in   std_logic_vector(3 downto 0);   -- digit points
            segment  : out std_logic_vector(7 downto 0);
            digit    : out std_logic_vector(3 downto 0)
           );
   end component;

   -- ChipScope Pro ICON
   component icon
      port (
            control0 : inout std_logic_vector(35 downto 0)
           );
   end component;

   -- ChipScope Pro VIO
   component vio
      port (
            control  : inout std_logic_vector(35 downto 0);
            clk      : in    std_logic;
            sync_out : out   std_logic_vector(22 downto 0)
           );
   end component;

begin

   -- instantiate the controller
   led_display_cntrl_inst: led_display_cntrl
   port map (
             rst      => rst,
             clk      => sys_clk,
             en       => en,
             test     => test,
             digit_0  => digit_0,
             digit_1  => digit_1,
             digit_2  => digit_2,
             digit_3  => digit_3,
             dp       => dp,
             segment  => segment,
             digit    => digit
            );

   -- instantiate ChipScope Pro ICON
   icon_inst: icon
      port map (
                control0 => control
               );

   -- instantiate ChipScope Pro VIO
   vio_inst: vio
      port map (
                control  => control,
                clk      => sys_clk,
                sync_out => vio_out
               );

   -- connect ChipScope Pro VIO output to the controller inputs
   rst     <= vio_out(22);
   en      <= vio_out(21);
   test    <= vio_out(20);
   dp      <= vio_out(19 downto 16);
   digit_3 <= vio_out(15 downto 12);
   digit_2 <= vio_out(11 downto  8);
   digit_1 <= vio_out( 7 downto  4);
   digit_0 <= vio_out( 3 downto  0);
   
end structural;

