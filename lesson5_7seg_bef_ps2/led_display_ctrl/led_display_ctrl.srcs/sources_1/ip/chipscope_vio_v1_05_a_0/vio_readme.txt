The following files were generated for 'vio' in directory
/home/student/FPGA/led_display_ctrl/led_display_ctrl.srcs/sources_1/ip/chipscope_vio_v1_05_a_0/

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * vio.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * vio.cdc
   * vio.constraints/vio.ucf
   * vio.constraints/vio.xdc
   * vio.ngc
   * vio.ucf
   * vio.vhd
   * vio.vho
   * vio.xdc
   * vio_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * vio.asy

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * vio.gise
   * vio.xise

Deliver Readme:
   Readme file for the IP.

   * vio_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * vio_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

