#ChipScope Core Generator Project File Version 3.0
#Tue Apr 09 23:23:07 CEST 2013
SignalExport.bus<0000>.channelList=0 1
SignalExport.bus<0000>.name=TRIG0
SignalExport.bus<0000>.offset=0.0
SignalExport.bus<0000>.precision=0
SignalExport.bus<0000>.radix=Bin
SignalExport.bus<0000>.scaleFactor=1.0
SignalExport.bus<0001>.channelList=2 3 4 5
SignalExport.bus<0001>.name=TRIG1
SignalExport.bus<0001>.offset=0.0
SignalExport.bus<0001>.precision=0
SignalExport.bus<0001>.radix=Bin
SignalExport.bus<0001>.scaleFactor=1.0
SignalExport.clockChannel=CLK
SignalExport.dataEqualsTrigger=true
SignalExport.triggerChannel<0000><0000>=TRIG0[0]
SignalExport.triggerChannel<0000><0001>=TRIG0[1]
SignalExport.triggerChannel<0001><0000>=TRIG1[0]
SignalExport.triggerChannel<0001><0001>=TRIG1[1]
SignalExport.triggerChannel<0001><0002>=TRIG1[2]
SignalExport.triggerChannel<0001><0003>=TRIG1[3]
SignalExport.triggerPort<0000>.name=TRIG0
SignalExport.triggerPort<0001>.name=TRIG1
SignalExport.triggerPortCount=2
SignalExport.triggerPortIsData<0000>=true
SignalExport.triggerPortIsData<0001>=true
SignalExport.triggerPortWidth<0000>=2
SignalExport.triggerPortWidth<0001>=4
SignalExport.type=ila

