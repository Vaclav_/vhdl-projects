The following files were generated for 'ila' in directory
/home/zsolti/FPGA_2013/OK/ps2_rx_test/ps2_rx_test.srcs/sources_1/ip/chipscope_ila_v1_05_a_0/

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * ila.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * ila.cdc
   * ila.constraints/ila.ucf
   * ila.constraints/ila.xdc
   * ila.ncf
   * ila.ngc
   * ila.ucf
   * ila.vhd
   * ila.vho
   * ila.xdc
   * ila_xmdf.tcl

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * ila.asy

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * ila.gise
   * ila.xise

Deliver Readme:
   Readme file for the IP.

   * ila_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * ila_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

