----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Zsolt Milotai
-- 
-- Create Date:    23:26:15 04/11/2012 
-- Design Name:    PS/2 receiver test
-- Module Name:    ps2_rx_test - structural 
-- Project Name:   PS/2 receiver
-- Target Devices: Spartan-6 Family (XC6SLX16 on Digilent Nexys 3 board)
-- Tool versions:  ISE Design Suite 13.2
-- Description:    This is a test for the PS/2 receiver.
--                 It has 8-bit counters for:
--                    - the number of 8-bit received data (rx_ready_cnt),
--                    - the number of parity errors occured (rx_parity_err_cnt),
--                    - the number of framing errors occured (rx_frame_err_cnt),
--                    - the number of WDT reset pulses (rx_wdt_rst_cnt).
--                 The receiver can be turned on and off by a ChipScope Pro VIO
--                 output channel. The 8-bit received data, the 8-bit counters
--                 and the receiver status flags can be monitored via a ChipScope
--                 Pro VIO input channel. The 8-bit received data also connected
--                 to the 8 LEDs of the board.
--                 In addition, there is a ChipScope Pro ILA for analyzing the
--                 PS/2 protocol signals and the receiver status flags.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ps2_rx_test is
   port (
         -- system clock & reset
         sys_clk  : in  std_logic;
         sys_rst  : in  std_logic;
         
         -- PS/2 signals
         ps2_clk  : in  std_logic;
         ps2_data : in  std_logic;
         
         -- 8-bit LEDs
         leds     : out std_logic_vector(7 downto 0)
        );
end ps2_rx_test;

architecture structural of ps2_rx_test is

   -- Constants

   -- frequency of the system clock in Hz
   constant sys_clk_freq : integer := 100_000_000;
   
   -- sampling frequency of the ChipScope Pro ILA in Hz
   constant ila_clk_freq : integer := 400_000;

   -- Signals

   -- PS/2 receiver signals
   signal rx_en          : std_logic;
   signal rx_wdt_rst     : std_logic;
   signal rx_data        : std_logic_vector(7 downto 0);
   signal rx_in_progress : std_logic;
   signal rx_ready       : std_logic;
   signal rx_parity_err  : std_logic;
   signal rx_frame_err   : std_logic;

   -- 8-bit counters for each flag
   signal rx_ready_cnt      : unsigned(7 downto 0) := X"00";
   signal rx_parity_err_cnt : unsigned(7 downto 0) := X"00";
   signal rx_frame_err_cnt  : unsigned(7 downto 0) := X"00";
   signal rx_wdt_rst_cnt    : unsigned(7 downto 0) := X"00";

   -- counter enable signals for each counter
   signal rx_ready_cnt_en      : std_logic := '1';
   signal rx_parity_err_cnt_en : std_logic := '1';
   signal rx_frame_err_cnt_en  : std_logic := '1';

   -- ChipScope Pro signals
   signal control0 : std_logic_vector(35 downto 0);
   signal control1 : std_logic_vector(35 downto 0);
   signal ila_clk  : std_logic;
   signal trig0    : std_logic_vector(1 downto 0);
   signal trig1    : std_logic_vector(3 downto 0);
   signal vio_in   : std_logic_vector(42 downto 0);
   signal vio_out  : std_logic_vector(0 downto 0);

   -- Component declarations

   -- PS/2 receiver
   component ps2_rx
      port (
            sys_clk        : in  std_logic;
            sys_rst        : in  std_logic;
            ps2_clk        : in  std_logic;
            ps2_data       : in  std_logic;
            rx_en          : in  std_logic;
            rx_wdt_rst     : out std_logic;
            rx_data        : out std_logic_vector(7 downto 0);
            rx_in_progress : out std_logic;
            rx_ready       : out std_logic;
            rx_parity_err  : out std_logic;
            rx_frame_err   : out std_logic
           );
   end component;

   -- clock division
   component clk_div
      generic (
               input_freq   : integer;
               desired_freq : integer
              );
      port (
            clk_in  : in  std_logic;
            clk_out : out std_logic
           );
   end component;

   -- ChipScope Pro ICON
   component icon
      port (
            control0 : inout std_logic_vector(35 downto 0);
            control1 : inout std_logic_vector(35 downto 0)
           );
   end component;

   -- ChipScope Pro ILA
   component ila
      port (
            control : inout std_logic_vector(35 downto 0);
            clk     : in    std_logic;
            trig0   : in    std_logic_vector(1 downto 0);
            trig1   : in    std_logic_vector(3 downto 0)
           );
   end component;

   -- ChipScope Pro VIO
   component vio
      port (
            control  : inout std_logic_vector(35 downto 0);
            clk      : in    std_logic;
            sync_in  : in    std_logic_vector(42 downto 0);
            sync_out : out   std_logic_vector(0 to 0)
           );
   end component;

begin

   -- instantiate the PS/2 receiver
   ps2_rx_inst: ps2_rx
   port map (
             sys_clk        => sys_clk,
             sys_rst        => sys_rst,
             ps2_clk        => ps2_clk,
             ps2_data       => ps2_data,
             rx_en          => rx_en,
             rx_wdt_rst     => rx_wdt_rst,
             rx_data        => rx_data,
             rx_in_progress => rx_in_progress,
             rx_ready       => rx_ready,
             rx_parity_err  => rx_parity_err,
             rx_frame_err   => rx_frame_err
            );

   -- instantiate clk_div to generate clock for the ChipScope Pro ILA
   clk_div_inst: clk_div
   generic map (
                input_freq   => sys_clk_freq,
                desired_freq => ila_clk_freq
               )
   port map (
             clk_in  => sys_clk,
             clk_out => ila_clk
            );

   -- instantiate ChipScope Pro ICON
   icon_inst: icon
   port map (
             control0 => control0,
             control1 => control1
            );

   -- instantiate ChipScope Pro ILA
   ila_inst: ila
   port map (
             control => control0,
             clk     => ila_clk,
             trig0   => trig0,
             trig1   => trig1
            );

   -- instantiate ChipScope Pro VIO
   vio_inst: vio
   port map (
             control  => control1,
             clk      => sys_clk,
             sync_in  => vio_in,
             sync_out => vio_out
            );

   -- connect PS/2 signals to the ChipScope Pro ILA
   trig0 <= ps2_clk & ps2_data;

   -- connect the receiver status signals to the ChipScope Pro ILA
   trig1 <= rx_in_progress & rx_ready & rx_parity_err & rx_frame_err;

   -- connect ChipScope Pro VIO output to the receiver enable input
   rx_en <= vio_out(0);

   -- connect the received data & counters & status flags to the ChipScope Pro VIO input
   vio_in(7 downto 0)   <= rx_data;
   vio_in(15 downto 8)  <= std_logic_vector(rx_ready_cnt);
   vio_in(23 downto 16) <= std_logic_vector(rx_parity_err_cnt);
   vio_in(31 downto 24) <= std_logic_vector(rx_frame_err_cnt);
   vio_in(39 downto 32) <= std_logic_vector(rx_wdt_rst_cnt);
   vio_in(40) <= rx_ready;
   vio_in(41) <= rx_parity_err;
   vio_in(42) <= rx_frame_err;

   -- put the received data to the LEDs
   leds <= rx_data;

   -- received frame counter
   rx_ready_cnt_proc: process(sys_clk)
   begin
      if (rising_edge(sys_clk)) then
         if (rx_ready = '1') then
            if (rx_ready_cnt_en = '1') then
               rx_ready_cnt <= rx_ready_cnt + 1;
               rx_ready_cnt_en <= '0';
            end if;
         else
            rx_ready_cnt_en <= '1';
         end if;
      end if;
   end process rx_ready_cnt_proc;

   -- parity error counter
   rx_parity_err_cnt_proc: process(sys_clk)
   begin
      if (rising_edge(sys_clk)) then
         if (rx_parity_err = '1') then
            if (rx_parity_err_cnt_en = '1') then
               rx_parity_err_cnt <= rx_parity_err_cnt + 1;
               rx_parity_err_cnt_en <= '0';
            end if;
         else
            rx_parity_err_cnt_en <= '1';
         end if;
      end if;
   end process rx_parity_err_cnt_proc;

   -- frame error counter
   rx_frame_err_cnt_proc: process(sys_clk)
   begin
      if (rising_edge(sys_clk)) then
         if (rx_frame_err = '1') then
            if (rx_frame_err_cnt_en = '1') then
               rx_frame_err_cnt <= rx_frame_err_cnt + 1;
               rx_frame_err_cnt_en <= '0';
            end if;
         else
            rx_frame_err_cnt_en <= '1';
         end if;
      end if;
   end process rx_frame_err_cnt_proc;

   -- WDT reset pulse counter
   rx_wdt_rst_cnt_proc: process(sys_clk)
   begin
      if (rising_edge(sys_clk)) then
         if (rx_wdt_rst = '1') then
            rx_wdt_rst_cnt <= rx_wdt_rst_cnt + 1;
         end if;
      end if;
   end process rx_wdt_rst_cnt_proc;

end structural;

