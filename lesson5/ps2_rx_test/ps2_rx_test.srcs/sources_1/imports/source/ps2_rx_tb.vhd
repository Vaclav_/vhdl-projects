--------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Zsolt Milotai
--
-- Create Date:    23:40:36 04/08/2012
-- Design Name:    PS/2 receiver test bench
-- Module Name:    ps2_rx_tb - behavioral
-- Project Name:   PS/2 receiver
-- Target Devices: Spartan-6 Family (XC6SLX16 on Digilent Nexys 3 board)
-- Tool versions:  ISE Design Suite 13.2
-- Description:    This is a test bench for the PS/2 receiver module.
-- 
-- VHDL Test Bench Created by ISE for module: ps2_rx
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity ps2_rx_tb is
end ps2_rx_tb;

architecture behavioral of ps2_rx_tb is 

   -- Component Declaration for the Unit Under Test (UUT)

   component ps2_rx
      port (
            sys_clk        : in  std_logic;
            sys_rst        : in  std_logic;
            ps2_clk        : in  std_logic;
            ps2_data       : in  std_logic;
            rx_en          : in  std_logic;
            rx_wdt_rst     : out std_logic;
            rx_data        : out std_logic_vector(7 downto 0);
            rx_in_progress : out std_logic;
            rx_ready       : out std_logic;
            rx_parity_err  : out std_logic;
            rx_frame_err   : out std_logic
           );
   end component;

   --Inputs
   signal sys_clk : std_logic := '0';
   signal sys_rst : std_logic := '0';
   signal ps2_clk : std_logic := '1';
   signal ps2_data : std_logic := '1';
   signal rx_en : std_logic := '0';

 	--Outputs
   signal rx_wdt_rst : std_logic;
   signal rx_data : std_logic_vector(7 downto 0);
   signal rx_in_progress : std_logic;
   signal rx_ready : std_logic;
   signal rx_parity_err : std_logic;
   signal rx_frame_err : std_logic;
   
   -- Number of the generated PS/2 frame
   constant ps2_data_frame_num : integer := 10;
   
   -- The PS/2 frames for simulation
   type ps2_data_frame_type is array(0 to ps2_data_frame_num - 1) of std_logic_vector(12 downto 0);
   constant ps2_data_frame : ps2_data_frame_type :=

       -- bit order: WaitFor2Cycles_Start_Data_Parity_Stop
      (
       B"11_0_01001001_0_1",    -- correct frame
       B"11_0_01101111_1_1",    -- correct frame
       B"11_0_11010000_1_1",    -- parity error
       B"11_0_01001001_0_1",    -- correct frame
       B"11_1_10111010_0_1",    -- frame error (start bit is wrong)
       B"11_0_01011001_1_1",    -- correct frame
       B"11_0_00101010_0_0",    -- frame error (stop bit is wrong)
       B"11_0_01101101_0_1",    -- correct frame
       B"11_1_00111000_1_0",    -- frame error (start & stop bits are wrong) & parity error
       B"11_0_11101001_0_1"     -- correct frame
      );

   -- Clocking of a complete PS/2 frame
   -- 1 1 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1
   -- _______   _   _   _   _   _   _   _   _   _   _   _
   --        |_| |_| |_| |_| |_| |_| |_| |_| |_| |_| |_|
   --      Start  D0  D1  D2  D3  D4  D5  D6  D7  P  Stop
   --
   constant ps2_clk_frame : std_logic_vector(25 downto 0) := B"1111_0101010101010101010101";

   -- Clock period definitions
   constant sys_clk_period : time := 10 ns;
   constant ps2_clk_period : time := 30 us;

begin

	-- Instantiate the Unit Under Test (UUT)
   uut: ps2_rx
   port map (
             sys_clk        => sys_clk,
             sys_rst        => sys_rst,
             ps2_clk        => ps2_clk,
             ps2_data       => ps2_data,
             rx_en          => rx_en,
             rx_wdt_rst     => rx_wdt_rst,
             rx_data        => rx_data,
             rx_in_progress => rx_in_progress,
             rx_ready       => rx_ready,
             rx_parity_err  => rx_parity_err,
             rx_frame_err   => rx_frame_err
            );

   -- Clock process definitions
   sys_clk_process: process
   begin
		sys_clk <= '0';
		wait for sys_clk_period/2;
		sys_clk <= '1';
		wait for sys_clk_period/2;
   end process;

   -- Generate PS/2 clock
   ps2_clk_gen_process: process
   begin
      wait for ps2_clk_period/2;
      for i in 0 to ps2_data_frame_num - 1 loop
         for j in 25 downto 0 loop
            ps2_clk <= ps2_clk_frame(j);
            wait for ps2_clk_period/2;
         end loop;
      end loop;
      wait;
   end process ps2_clk_gen_process;

   -- Send PS/2 data frames
   ps2_data_send_proc: process
   begin
      for i in 0 to ps2_data_frame_num - 1 loop
         for j in 12 downto 0 loop
            ps2_data <= ps2_data_frame(i)(j);
            wait for ps2_clk_period;
         end loop;
      end loop;
      wait;
   end process ps2_data_send_proc;

   -- Stimulus process
   stim_proc: process
   begin
      sys_rst <= '1',
                 '0' after 300 ns;
      rx_en <= '0',
               '1' after 400 ns;
      wait;
   end process;

end behavioral;
