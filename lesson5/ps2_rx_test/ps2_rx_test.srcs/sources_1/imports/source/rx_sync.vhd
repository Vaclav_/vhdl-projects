----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Zsolt Milotai
-- 
-- Create Date:    23:51:17 12/08/2011 
-- Design Name:    Rx synchronizer
-- Module Name:    rx_sync - behavioral 
-- Project Name:   PS/2 receiver
-- Target Devices: Spartan-6 Family (XC6SLX16 on Digilent Nexys 3 board)
-- Tool versions:  ISE Design Suite 13.2
-- Description:    This module is a synchronous serial (1 clock + 1 data line)
--                 bus synchronizer.
--                 It works only if the system clock frequency is greater than
--                 the bus clock frequency.
--                 It captures data on the falling edge of the bus clock.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity rx_sync is
   port (
         -- system clock
         sys_clk   : in  std_logic;
         
         -- asynchronous clock & data from outside
         asy_clk   : in  std_logic;
         asy_data  : in  std_logic;
         
         -- synchronized clock & data to the system clock
         sync_clk  : out std_logic;
         sync_data : out std_logic
        );
end rx_sync;

architecture behavioral of rx_sync is

   signal sync_clk_ff_1 : std_logic;
   signal sync_clk_ff_2 : std_logic;
   signal captured_data : std_logic;

begin

   -- capture input data on falling edge of asy_clk
   capture_proc: process(asy_clk)
   begin
      if (falling_edge(asy_clk)) then
         captured_data <= asy_data;
      end if;
   end process capture_proc;

   -- synchronize captured data to the system clock
   sync_proc: process(sys_clk)
   begin
      if (rising_edge(sys_clk)) then

         sync_clk_ff_1 <= asy_clk;
         sync_clk_ff_2 <= sync_clk_ff_1;
         sync_clk <= '0';

         -- detect falling edge of asy_clk
         if (sync_clk_ff_1 = '0' and sync_clk_ff_2 = '1') then
            sync_data <= captured_data;
            sync_clk <= '1';
         end if;

      end if;
   end process sync_proc;

end behavioral;

