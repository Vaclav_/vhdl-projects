----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Zsolt Milotai
-- 
-- Create Date:    22:01:13 11/30/2011 
-- Design Name:    PS/2 receiver
-- Module Name:    ps2_rx - behavioral 
-- Project Name:   PS/2 receiver
-- Target Devices: Spartan-6 Family (XC6SLX16 on Digilent Nexys 3 board)
-- Tool versions:  ISE Design Suite 13.2
-- Description:    This module is a PS/2 receiver with double data buffering
--                 support.
--                 Before any processing the arrived synchronous serial data has
--                 been synchronized to the system clock through the 'rx_sync'
--                 synchronizer component.
--                 The 'rx_ready' status flag always indicate that the actual
--                 receiving is complete. During the transmission if a parity or
--                 a framing error occurs, the 'rx_parity_err' or the
--                 'rx_frame_err' flags will indicate the error respectively.
--                 All flags are active until the next data byte comes.
--                 An 8-bit received data is valid on the output until the
--                 transmission of the next data byte has finished regardless of
--                 any errors. It ensures that the data on the output and the
--                 status flags are always consistent with each other.
--                 The 'rx_in_progress' output indicates that the transmission
--                 have began but not finished yet. It is useful, because during
--                 the transfer it inhibits for the transmitter on this end to
--                 mess the line.
--                 Furthermore, a Watchdog Timer (WDT) can watch the PS/2 receiver
--                 since the receiver periodically send a pulse during the
--                 transmission on its 'rx_wdt_rst' line in order to reset the WDT
--                 when a bit arrives. If a clock pulse lost on the ps2_clk line
--                 during a transmission resulting an incomplete frame, the WDT
--                 will reset the receiver.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: For more information about the PS/2 protocol, please
--                      see: http://www.computer-engineering.org/ps2protocol/
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ps2_rx is
   port (
         -- system clock & reset
         sys_clk        : in  std_logic;
         sys_rst        : in  std_logic;

         -- PS/2 signals
         ps2_clk        : in  std_logic;
         ps2_data       : in  std_logic;

         -- receiver enable
         rx_en          : in  std_logic;

         -- receiver's reset output for Watchdog Timer
         rx_wdt_rst     : out std_logic;

         -- received data (8-bits)
         rx_data        : out std_logic_vector(7 downto 0);

         -- receiving is in progress
         rx_in_progress : out std_logic;

         -- status flags (all is registered)
         rx_ready       : out std_logic;
         rx_parity_err  : out std_logic;
         rx_frame_err   : out std_logic
        );
end ps2_rx;

architecture behavioral of ps2_rx is

   -- Type declarations

   -- receiver FSM states
   type fsm_state_type is (start, data, par, stop);

   -- 8-bit double data buffer
   type data_buf_type is array (1 downto 0) of std_logic_vector(7 downto 0);

   -- Constants

   -- data bits defined in PS/2 protocol
   constant ps2_data_bit_num : integer := 8;

   -- Signals

   -- synchronized data and enable
   signal enable    : std_logic;
   signal sync_data : std_logic;

   -- FSM signals
   signal state      : fsm_state_type;
   signal next_state : fsm_state_type;
   signal shift_cnt  : unsigned(2 downto 0);

   -- internal parity
   signal parity      : std_logic;

   -- data buffer
   signal data_buffer : data_buf_type := (others => (others => '0'));

   -- Component declarations

   -- synchronizer
   component rx_sync
      port (
            sys_clk   : in  std_logic;
            asy_clk   : in  std_logic;
            asy_data  : in  std_logic;
            sync_clk  : out std_logic;
            sync_data : out std_logic
           );
   end component;

begin

   -- instantiate synchronization circuit
   rx_sync_inst: rx_sync
   port map (
             sys_clk   => sys_clk,
             asy_clk   => ps2_clk,
             asy_data  => ps2_data,
             sync_clk  => enable,
             sync_data => sync_data
            );

   -- FSM state memory
   state_proc: process(sys_clk)
   begin
      if (rising_edge(sys_clk)) then
         if (sys_rst = '1' or rx_en = '0') then
            state <= stop;
            rx_wdt_rst <= '0';
         elsif (enable = '1') then
            state <= next_state;
            rx_wdt_rst <= '1';
         else
            rx_wdt_rst <= '0';
         end if;
      end if;
   end process state_proc;

   -- FSM next-state logic
   next_state_proc: process(state, shift_cnt)
   begin
      case state is
         when start =>
            next_state <= data;
         when data =>
            if (shift_cnt = 0) then
               next_state <= par;
            else
               next_state <= data;
            end if;
         when par =>
            next_state <= stop;
         when stop =>
            next_state <= start;
      end case;
   end process next_state_proc;

   -- data bit shift counter
   shift_cnt_proc: process(sys_clk)
   begin
      if (rising_edge(sys_clk)) then
         if (sys_rst = '1' or rx_en = '0') then
            shift_cnt <= to_unsigned(ps2_data_bit_num - 1, 3);
         elsif (state = data and enable = '1') then
            if (shift_cnt = 0) then
               shift_cnt <= to_unsigned(ps2_data_bit_num - 1, 3);
            else
               shift_cnt <= shift_cnt - 1;
            end if;
         end if;
      end if;
   end process shift_cnt_proc;
   
   -- set status flags
   rx_status_proc: process(sys_clk)
   begin
      if (rising_edge(sys_clk)) then
         if (sys_rst = '1') then
            rx_in_progress <= '0';
            rx_ready <= '0';
            rx_frame_err <= '0';
            rx_parity_err <= '0';
         elsif (rx_en = '1' and enable = '1') then
            case next_state is
               when start =>
                  rx_ready <= '0';
                  rx_parity_err <= '0';
                  rx_in_progress <= '1';

                  -- if start bit = 0 -> no frame error
                  -- if start bit = 1 -> frame error
                  rx_frame_err <= sync_data;
                  
               when par =>
                  -- registrate parity in the right time
                  rx_parity_err <= not parity;
                  
               when stop =>
                  rx_ready <= '1';
                  rx_in_progress <= '0';
                  
                  -- if stop bit = 1 -> no frame error
                  -- if stop bit = 0 -> frame error
                  rx_frame_err <= not sync_data;
                  
               when others =>
                  null;
            end case;
         end if;
      end if;
   end process rx_status_proc;

   -- generate parity from 8-bit registered data & the incoming data bit
   parity <= data_buffer(0)(7) xor
             data_buffer(0)(6) xor
             data_buffer(0)(5) xor
             data_buffer(0)(4) xor
             data_buffer(0)(3) xor
             data_buffer(0)(2) xor
             data_buffer(0)(1) xor
             data_buffer(0)(0) xor
             sync_data;

   -- shift incoming data into the buffer
   shift_proc: process(sys_clk)
   begin
      if (rising_edge(sys_clk)) then
         if (enable = '1' and next_state = data) then
            data_buffer(0) <= sync_data & data_buffer(0)(7 downto 1);
         end if;
      end if;
   end process shift_proc;

   -- double buffering
   frame_err_proc: process(sys_clk)
   begin
      if (rising_edge(sys_clk)) then
         if (sys_rst = '1') then
            data_buffer(1) <= (others => '0');
         elsif (enable = '1' and state = par) then
            data_buffer(1) <= data_buffer(0);
         end if;
      end if;
   end process frame_err_proc;

   -- only data_buffer(1) is available for the user
   rx_data <= data_buffer(1);

end behavioral;

