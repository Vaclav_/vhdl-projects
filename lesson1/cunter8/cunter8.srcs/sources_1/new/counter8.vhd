----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.03.2013 17:53:56
-- Design Name: 
-- Module Name: counter8 - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;        --itt van az STD_LOGIC es az STD_LOGIC_VECTOR definialva
use IEEE.NUMERIC_STD.ALL;       --aritmetikai muveletek
--library UNISIM;
--use UNISIM.VComponents.all;       --FPGA primitivekhez kell, pl LUT

entity counter8 is
    Port ( clk : in STD_LOGIC;
           --clk, rst, en, load : in STD_LOGIC; -- igy is lehet
           rst : in STD_LOGIC;
           en : in STD_LOGIC;
           load : in STD_LOGIC;
           din : in STD_LOGIC_VECTOR (7 downto 0);
           cnt : out STD_LOGIC_VECTOR (7 downto 0)
           );
end counter8;

architecture behavioral of counter8 is
--begin elo�tt vannak a deklaraciok
    
    signal q_cnt : unsigned(7 downto 0);
    --q_ regiszter i_ bemenet o_ kimenet w_ wire v_ valtozo c_ kombinacios halozat kimenete
    
begin
--a mukodes leirasa

    cnt <= STD_LOGIC_VECTOR(q_cnt);

    proc_counter : process( clk )    --cimke, zarojel: erzekenysegi lista, jelek lehetnek itt
                                     --amiknek a megvaltozasa kivaltja a process futasat
    begin
        if( rising_edge(clk) ) then
            if( rst = '1' ) then
                q_cnt <= x"00";             -- <= ertekadas
            elsif( en = '1' ) then
                if( load = '1' ) then
                    q_cnt <= unsigned(din);     --a VHDL erosen tipusos nyelv, ezert kell a castolas
                else
                    --cnt <= cnt + 1; de: nem tudjuk a kimenetet olvasni
                    q_cnt <= q_cnt + 1;
                end if;
            end if;        
        end if;
    end process proc_counter;
    
end behavioral;
