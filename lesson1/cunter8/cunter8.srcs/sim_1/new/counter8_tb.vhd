----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.03.2013 18:42:21
-- Design Name: 
-- Module Name: counter8_tb - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter8_tb is
   -- Port ( );
end counter8_tb;

architecture behavioral of counter8_tb is

    --vezetékek
    signal clk, rst, en, load : STD_LOGIC;
    signal din, cnt : STD_LOGIC_VECTOR(7 downto 0);
    
    component counter8
        Port ( clk : in STD_LOGIC;
               --clk, rst, en, load : in STD_LOGIC; -- így is lehet
               rst : in STD_LOGIC;  -- ; -> elválasztás, blokkzáró, az utolso után nem kell
               en : in STD_LOGIC;
               load : in STD_LOGIC;
               din : in STD_LOGIC_VECTOR (7 downto 0);
               cnt : out STD_LOGIC_VECTOR (7 downto 0)
               );
    end component counter8;

begin
    --itt példányosítjuk
    uut : counter8
    port map (  clk => clk, --rendeld hozzá
                rst => rst, --counter8 rst (port)=> testbenchben lévő rst (vezeték) 
                en => en,
                load => load,
                din => din,
                cnt => cnt
                ); --asszociációs lista

    proc_clk_gen : process --nincs érzékenységi listája
    begin
        clk <= '0'; wait for 10 ns;
        clk <= '1'; wait for 10 ns;    
    end process proc_clk_gen;
    
    proc_stimulus : process
    begin
        rst <= '1',
               '0' after 40 ns; --1 után 40 ns al 0 lesz
               
        en <= '1',
              '0' after 200 ns,
              '1' after 300 ns;
              
       load <= '0',
               '1' after 250 ns,
               '0' after 270 ns,
               '1' after 400 ns,
               '0' after 420 ns;
       din <= x"42";
    wait;   --várakozás végtelen ideig
    end process proc_stimulus;
    
end behavioral;
