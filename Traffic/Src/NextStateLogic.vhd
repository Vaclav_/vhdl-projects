----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 12.04.2013 23:40:39
-- Project Name: Traffic
-- Module Name: NextStateLogic - Behavioral
-- Description: next state logika, a traffic_ctrl.vhd hasznalja fel
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity NextStateLogic is
    Port (
		i_ctrlON : in STD_LOGIC;
		i_curLedState : in STD_LOGIC_VECTOR (7 downto 0);
		o_nextLedState : out STD_LOGIC_VECTOR (7 downto 0)
	);
end NextStateLogic;

architecture Behavioral of NextStateLogic is

	constant stateLedRST : STD_LOGIC_VECTOR(7 downto 0) := x"ff";
	constant stateLedOFF : STD_LOGIC_VECTOR(7 downto 0) := x"20";		--default
	constant stateLedRED : STD_LOGIC_VECTOR(7 downto 0) := x"10";
	constant stateLed1End : STD_LOGIC_VECTOR(7 downto 0) := x"03";
	constant stateLed2End : STD_LOGIC_VECTOR(7 downto 0) := x"07";
	constant stateLed3End : STD_LOGIC_VECTOR(7 downto 0) := x"0b";
	constant stateLed4End : STD_LOGIC_VECTOR(7 downto 0) := x"0f";
	constant stateLed1First : STD_LOGIC_VECTOR(7 downto 0) := x"00";

begin
	
	p_ledState : process( i_curLedState, i_ctrlON )
	begin
		case i_curLedState is
			--always return to OFF state from reset
			when stateLedRST =>
				o_nextLedState <= stateLedOFF; 
			when stateLedOFF =>
				if( i_ctrlON = '1' ) then
					--start turning on
					o_nextLedState <= stateLedRED;
				else
					--no change
					o_nextLedState <= stateLedOFF;
				end if;
			when stateLedRED =>
				if( i_ctrlON = '1' ) then
					--turn on
					o_nextLedState <= stateLed1First;
				else
					--turn off
					o_nextLedState <= stateLedOFF;
				end if;
			when stateLed1End =>
				if( i_ctrlON = '1' ) then
					o_nextLedState <= std_logic_vector( unsigned(i_curLedState) + 1 );
				else
					--start turning off
					o_nextLedState <= stateLedRED;
				end if;
			when stateLed2End =>
				if( i_ctrlON = '1' ) then
					o_nextLedState <= std_logic_vector( unsigned(i_curLedState) + 1 );
				else
					--start turning off
					o_nextLedState <= stateLedRED;
				end if;
			when stateLed3End =>
				if( i_ctrlON = '1' ) then
					o_nextLedState <= std_logic_vector( unsigned(i_curLedState) + 1 );
				else
					--start turning off
					o_nextLedState <= stateLedRED;
			end if;
			when stateLed4End =>
				if( i_ctrlON = '1' ) then
					--start the loop over
					o_nextLedState <= stateLed1First;
				else
					--start turning off
					o_nextLedState <= stateLedRED;
			end if;
			when others =>
				if( i_curLedState < x"10" ) then
					--next state
					o_nextLedState <= std_logic_vector( unsigned(i_curLedState) + 1 );
				else
					--error
					o_nextLedState <= x"ff";
				end if;
		end case;
	end process p_ledState;

end Behavioral;
