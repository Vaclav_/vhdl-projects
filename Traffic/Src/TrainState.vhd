----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 13.04.2013 12:06:53
-- Project Name: Traffic
-- Module Name: TrainState - Behavioral
-- Description:
--		a vasuti lampak allapotait kezeli, a traffic_ctrl.vhd hasznalja fel
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TrainState is
    Port (
    	i_trainL : in STD_LOGIC;
		i_trainR : in STD_LOGIC;
		i_en : in STD_LOGIC;
		i_rst : in STD_LOGIC;
		o_curTrainState : out STD_LOGIC_VECTOR(1 downto 0)
	);
end TrainState;

architecture Behavioral of TrainState is

	constant stateTrainRST : STD_LOGIC_VECTOR(1 downto 0) := b"11";
	constant stateTrainIdle : STD_LOGIC_VECTOR(1 downto 0) := b"00";	--default
	constant stateTrainLeft : STD_LOGIC_VECTOR(1 downto 0) := b"10";
	constant stateTrainRight : STD_LOGIC_VECTOR(1 downto 0) := b"01";
	
	signal c_TrainState : STD_LOGIC_VECTOR(1 downto 0) := stateTrainIdle;
	signal trainPast : STD_LOGIC := '1';
	
	signal prevTrainState : STD_LOGIC_VECTOR(1 downto 0) := stateTrainIdle;
	signal prevTrainPast : STD_LOGIC := '1';

begin

	p_trainState : process( c_TrainState, i_en, i_rst, i_trainL, i_trainR )
	begin
		if( i_rst = '1' ) then
			trainPast <= '1';
			c_TrainState <= stateTrainRST;
		else
			if( i_en = '1' ) then
				case c_TrainState is
					when b"00" =>
						if( trainPast = '0' ) then
							if( (i_trainL = '0') and (i_trainR = '0') ) then
								--it's past
								trainPast <= '1';
								c_TrainState <= b"00";
							else
								--hold
								trainPast <= '0';
								c_TrainState <= b"00";
							end if;
						else
							--train from the left
							if( i_trainL = '1' ) then
								trainPast <= '0';
								c_TrainState <= b"10";
							else
								--train from the right
								if( i_trainR = '1' ) then
									trainPast <= '0';
									c_TrainState <= b"01";
								else	--no train, no action
									trainPast <= '1';
									c_TrainState <= b"00";
								end if;
							end if;
						end if;
					when b"10" =>	--train from the left
						--train left if !Left and Right
						if( (i_trainL = '0') and (i_trainR = '1') ) then
							--the train is gone, but not past yet
							trainPast <= '0';
							c_TrainState <= b"00";
						else
							--hold state
							trainPast <= '0';
							c_TrainState <= b"10";
						end if;
					when b"01" =>	--train from the right
						--train left if Left and !Right
						if( (i_trainL = '1') and (i_trainR = '0') ) then
							--the train is gone, but not past yet
							trainPast <= '0';
							c_TrainState <= b"00";
						else
							--hold state
							trainPast <= '0';
							c_TrainState <= b"01";
						end if;
					when others =>	--this state is after reset
						--train from the left
						if( i_trainL = '1' ) then
							trainPast <= '0';
							c_TrainState <= b"10";
						else
							--train from the right
							if( i_trainR = '1' ) then
								trainPast <= '0';
								c_TrainState <= b"01";
							else	--no train
								trainPast <= '1';
								c_TrainState <= b"00";
							end if;
						end if;
				end case;
			else	--not enabled
				--hold previous value
				c_TrainState <= prevTrainState;
				trainPast <= prevTrainPast;
			end if;
		end if;
		
		--save previous values
		prevTrainPast <= trainPast;
		prevTrainState <= c_TrainState;
		--put to output
		o_curTrainState <= c_TrainState;
		
	end process p_trainState;

end Behavioral;
