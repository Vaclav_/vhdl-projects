----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 12.04.2013 14:30:11
-- Project Name: Traffic
-- Module Name: StateMemory - Behavioral
-- Description:
--		a keresztezodes jelenlegi allapotat tarolja
--		a kovetkezo allapotot csak a megfelelo ido letelte utan ovlassa be
--		traffic_ctrl.vhd hasznalja fel
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity StateMemory is
    Port (
    	i_nextLedState : in STD_LOGIC_VECTOR (7 downto 0);
		i_en : in STD_LOGIC;
		i_rst : in STD_LOGIC;
		i_clk : in STD_LOGIC;
		o_curLedState : out STD_LOGIC_VECTOR (7 downto 0)
	);
end StateMemory;

architecture Behavioral of StateMemory is

	constant stateLedRST : STD_LOGIC_VECTOR(7 downto 0) := x"ff";
	constant stateLedOFF : STD_LOGIC_VECTOR(7 downto 0) := x"20";		--default


	signal c_nextLedState : STD_LOGIC_VECTOR(7 downto 0) := stateLedOFF;
	
	type type_timeRatio is array (0 to 16) of integer;--std_logic_vector(7 downto 0);
	constant timeRatio : type_timeRatio := (
		8, 32, 8, 8,	--stateLed1
		8, 32, 8, 8,	--stateLed2
		8, 24, 8, 8,	--stateLed3
		8, 32, 8, 8,	--stateLed4
		24				--stateRED
	);

	signal timeCounter : integer := 0;

	subtype type_stateNum is integer range 0 to 16;
	signal stateNum : type_stateNum := 0; 

begin

	p_setOutput : process( i_clk, i_en, i_rst, c_nextLedState )
	begin
		if( i_rst = '1' ) then
			o_curLedState <= stateLedRST;
			c_nextLedState <= stateLedOFF;		--the default state after reset
			timeCounter <= 0;
		else
			
			if( rising_edge(i_clk) and (i_en = '1') ) then
				--if not turned off
				if( c_nextLedState < stateLedOFF ) then
					case c_nextLedState is
						when x"00" => stateNum <= 0;
						when x"01" => stateNum <= 1;
						when x"02" => stateNum <= 2;
						when x"03" => stateNum <= 3;
						when x"04" => stateNum <= 4;
						when x"05" => stateNum <= 5;
						when x"06" => stateNum <= 6;
						when x"07" => stateNum <= 7;
						when x"08" => stateNum <= 8;
						when x"09" => stateNum <= 9;
						when x"0a" => stateNum <= 10;
						when x"0b" => stateNum <= 11;
						when x"0c" => stateNum <= 12;
						when x"0d" => stateNum <= 13;
						when x"0e" => stateNum <= 14;
						when x"0f" => stateNum <= 15;
						when x"10" => stateNum <= 16;
						when others => stateNum<= 16;
					end case;

					--time in this state is up
					if( timeCounter >= timeRatio( stateNum ) ) then
						c_nextLedState <= i_nextLedState;
						timeCounter <= 0;
					else	--stay in this state and count
						timeCounter <= timeCounter + 1;
					end if;
					
				else	--turned off -> react to every clock
					c_nextLedState <= i_nextLedState;
				end if;
			end if;		--end if( rising_edge(i_clk) and (i_en = '1') ) then
			o_curLedState <= c_nextLedState;
		end if;
	end process p_setOutput;

end Behavioral;
