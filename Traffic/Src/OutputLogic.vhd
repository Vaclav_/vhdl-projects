----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 22.04.2013 09:55:35
-- Project Name: Traffic
-- Module Name: OutputLogic - Behavioral
-- Description:
--		a lampakat mukodteti a jelenlegi allapot alapjan
--		az orajel a villogo zold es a vasuti lampakhoz kell
--		a traffic_ctrl.vhd hasznalja fel
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity OutputLogic is
    Port (
    	i_curTrainState : in STD_LOGIC_VECTOR(1 downto 0);
    	i_curLedState : in STD_LOGIC_VECTOR(7 downto 0);
    	i_test : in STD_LOGIC;
    	i_rst : in STD_LOGIC;
    	i_clk : in STD_LOGIC;
    	i_en : in STD_LOGIC;
    	
    	o_a1 : out STD_LOGIC;
    	o_b1 : out STD_LOGIC_VECTOR(2 downto 0);
    	o_c1 : out STD_LOGIC_VECTOR(2 downto 0);
    	o_d1 : out STD_LOGIC_VECTOR(1 downto 0);
    	o_a2 : out STD_LOGIC_VECTOR(2 downto 0);
		o_d2 : out STD_LOGIC_VECTOR(1 downto 0);
		o_a3 : out STD_LOGIC_VECTOR(2 downto 0);
		o_a4 : out STD_LOGIC;
		o_b4 : out STD_LOGIC_VECTOR(2 downto 0);
		
		o_trainLedL : out STD_LOGIC;
		o_trainLedR : out STD_LOGIC;
		o_trainWhite : out STD_LOGIC
    );
end OutputLogic;

architecture Behavioral of OutputLogic is

	signal s_flashState : STD_LOGIC := '0';

	signal stateIndex : natural := 0;
	
	signal m_a1 : STD_LOGIC := '0';
	signal m_b1 : STD_LOGIC_VECTOR(2 downto 0) := b"000";
	signal m_c1 : STD_LOGIC_VECTOR(2 downto 0) := b"000";
	signal m_d1 : STD_LOGIC_VECTOR(1 downto 0) := b"00";
	signal m_a2 : STD_LOGIC_VECTOR(2 downto 0) := b"000";
	signal m_d2 : STD_LOGIC_VECTOR(1 downto 0) := b"00";
	signal m_a3 : STD_LOGIC_VECTOR(2 downto 0) := b"000";
	signal m_a4 : STD_LOGIC := '0';
	signal m_b4 : STD_LOGIC_VECTOR(2 downto 0) := b"000";
	signal m_trainLedL : STD_LOGIC := '0';
	signal m_trainLedR : STD_LOGIC := '0';
	signal m_trainWhite : STD_LOGIC := '0';
	
	constant a1lut  : STD_LOGIC_VECTOR(15 downto 0) := b"0110000000000110";
	constant b1lut1 : STD_LOGIC_VECTOR(15 downto 0) := b"1001111111111111";	--red
	constant b1lut2 : STD_LOGIC_VECTOR(15 downto 0) := b"0101000000000000";	--yellow
	constant b1lut3 : STD_LOGIC_VECTOR(15 downto 0) := b"0010000000000000"; --green
	constant c1lut1 : STD_LOGIC_VECTOR(15 downto 0) := b"1111100111111111";
	constant c1lut2 : STD_LOGIC_VECTOR(15 downto 0) := b"0000010100000000";
	constant c1lut3 : STD_LOGIC_VECTOR(15 downto 0) := b"0000001000000000";
	constant d1lut1 : STD_LOGIC_VECTOR(15 downto 0) := b"1111111110011111";
	constant d1lut2 : STD_LOGIC_VECTOR(15 downto 0) := b"0000000001100000";
	constant a2lut1 : STD_LOGIC_VECTOR(15 downto 0) := b"1111111110011111";
	constant a2lut2 : STD_LOGIC_VECTOR(15 downto 0) := b"0000000001010000";
	constant a2lut3 : STD_LOGIC_VECTOR(15 downto 0) := b"0000000000100000";
	constant d2lut1 : STD_LOGIC_VECTOR(15 downto 0) := b"1001111111111111";
	constant d2lut2 : STD_LOGIC_VECTOR(15 downto 0) := b"0110000000000000";
	constant a3lut1 : STD_LOGIC_VECTOR(15 downto 0) := b"1001111111111111";
	constant a3lut2 : STD_LOGIC_VECTOR(15 downto 0) := b"0101000000000000";
	constant a3lut3 : STD_LOGIC_VECTOR(15 downto 0) := b"0010000000000000";
	constant a4lut  : STD_LOGIC_VECTOR(15 downto 0) := b"0000011111100000";
	constant b4lut1 : STD_LOGIC_VECTOR(15 downto 0) := b"1111111111111001";
	constant b4lut2 : STD_LOGIC_VECTOR(15 downto 0) := b"0000000000000101";
	constant b4lut3 : STD_LOGIC_VECTOR(15 downto 0) := b"0000000000000010";
	
	constant stateLedRST : STD_LOGIC_VECTOR(7 downto 0) := x"ff";
	constant stateLedOFF : STD_LOGIC_VECTOR(7 downto 0) := x"20";		--default
	constant stateLedRED : STD_LOGIC_VECTOR(7 downto 0) := x"10";
	constant stateLed1End : STD_LOGIC_VECTOR(7 downto 0) := x"03";
	constant stateLed2End : STD_LOGIC_VECTOR(7 downto 0) := x"07";
	constant stateLed3End : STD_LOGIC_VECTOR(7 downto 0) := x"0b";
	constant stateLed4End : STD_LOGIC_VECTOR(7 downto 0) := x"0f";
	constant stateLed1First : STD_LOGIC_VECTOR(7 downto 0) := x"00";

begin


	p_outputMux : process( 	i_test, i_rst, m_a1, m_b1, m_c1, m_d1, m_a2, 
							m_d2, m_a3, m_a4, m_b4, m_trainLedL, m_trainLedR,
							m_trainWhite )
	begin
		if( i_rst = '1' ) then
			o_a1 <= '0';
			o_b1 <= b"000";
			o_c1 <= b"000";
			o_d1 <= b"00";
			o_a2 <= b"000";
			o_d2 <= b"00";
			o_a3 <= b"000";
			o_a4 <= '0';
			o_b4 <= b"000";
			o_trainLedL <= '0';
			o_trainLedR <= '0';
			o_trainWhite <= '0';
		elsif( i_test = '1' ) then
			o_a1 <= '1';
			o_b1 <= b"111";
			o_c1 <= b"111";
			o_d1 <= b"11";
			o_a2 <= b"111";
			o_d2 <= b"11";
			o_a3 <= b"111";
			o_a4 <= '1';
			o_b4 <= b"111";
			o_trainLedL <= '1';
			o_trainLedR <= '1';
			o_trainWhite <= '1';
		else
			o_a1 <= m_a1;
			o_b1 <= m_b1;
			o_c1 <= m_c1;
			o_d1 <= m_d1;
			o_a2 <= m_a2;
			o_d2 <= m_d2;
			o_a3 <= m_a3;
			o_a4 <= m_a4;
			o_b4 <= m_b4;
			o_trainLedL <= m_trainLedL;
			o_trainLedR <= m_trainLedR;
			o_trainWhite <= m_trainWhite;
		end if;
	end process p_outputMux;

	p_flashing : process( i_clk )
	begin

		
		if( rising_edge(i_clk) ) then
		  if( i_en = '1' ) then
                s_flashState <= not s_flashState;
                if( i_curLedState <= x"0f" ) then
                    stateIndex <= to_integer(unsigned(i_curLedState));
                else
                    stateIndex <= 0;
                end if;
            else
                s_flashState <= s_flashState;
                stateIndex <= stateIndex;
			end if;
		end if;
		
	end process p_flashing;
	
	p_trainLogic : process( i_curTrainState, s_flashState )
	begin
	
		--nem jon vonat
		if( i_curTrainState = b"00" ) then
			--feher lampa villog
			m_trainWhite <= s_flashState;
			m_trainLedL <= '0';
			m_trainLedR <= '0';
		elsif( (i_curTrainState = b"01") or (i_curTrainState = b"10") ) then
			m_trainWhite <= '0';
			m_trainLedL <= s_flashState;
			m_trainLedR <= not s_flashState;
		else
			m_trainWhite <= '1';
			m_trainLedL <= '1';
			m_trainLedR <= '1';
			end if;
	end process p_trainLogic;
	
	p_ledLogic : process( i_curLedState, s_flashState )
	begin
		--sarga villogas
		if( i_curLedState = stateLedOFF ) then
			m_a1 <= '0';
			m_b1 <= '0' & s_flashState & '0';	
			m_c1 <= '0' & s_flashState & '0';	
			m_d1 <= b"00";
			m_a2 <= '0' & s_flashState & '0';
			m_d2 <= b"00";
			m_a3 <= '0' & s_flashState & '0';
			m_a4 <= '0';
			m_b4 <= '0' & s_flashState & '0';
		--osszes lampa piros
		elsif( i_curLedState = stateLedRED ) then
			m_a1 <= '0';
			m_b1 <= b"100";	
			m_c1 <= b"100";	
			m_d1 <= b"10";
			m_a2 <= b"100";
			m_d2 <= b"10";
			m_a3 <= b"100";
			m_a4 <= '0';
			m_b4 <= b"100";
		--allapotok 0-15ig
		elsif( i_curLedState <= x"0f" ) then
			m_a1 <= a1lut(stateIndex);
			
			m_c1(2) <= c1lut1(stateIndex);
			m_c1(1) <= c1lut2(stateIndex);
			m_c1(0) <= c1lut3(stateIndex);

			m_d1(1) <= d1lut1(stateIndex);
			m_a2(2) <= a2lut1(stateIndex);
			m_a2(1) <= a2lut2(stateIndex);
			m_a2(0) <= a2lut3(stateIndex);
			
			m_d2(1) <= d2lut1(stateIndex);
			m_a3(2) <= a3lut1(stateIndex);
			m_a3(1) <= a3lut2(stateIndex);
			m_a3(0) <= a3lut3(stateIndex);
			
			m_b4(2) <= b4lut1(stateIndex);
			m_b4(1) <= b4lut2(stateIndex);
			m_b4(0) <= b4lut3(stateIndex);
			--zebra -> villogo zold
			if( i_curLedState = x"06" ) then
				m_d1(0) <= s_flashState;
			else
				m_d1(0) <= d1lut2(stateIndex);
			end if;
			
			if( i_curLedState = x"0e" ) then
				m_d2(0) <= s_flashState;
			else
				m_d2(0) <= d2lut2(stateIndex);
			end if;
			
			--lampa letiltas, ha jon a vonat
			if( i_curTrainState = b"00" ) then
				m_b1(2) <= b1lut1(stateIndex);
				m_b1(1) <= b1lut2(stateIndex);
				m_b1(0) <= b1lut3(stateIndex);
				m_a4 <= a4lut(stateIndex);
			else
				--piros
				m_b1(2) <= '1';
				m_b1(1) <= '0';
				m_b1(0) <= '0';
				--kisivbe zold kikapcsolva
				m_a4 <= '0';
			end if;
		else
			m_a1 <= '1';
			m_b1 <= b"111";
			m_c1 <= b"111";
			m_d1 <= b"11";
			m_a2 <= b"111";
			m_d2 <= b"11";
			m_a3 <= b"111";
			m_a4 <= '1';
			m_b4 <= b"111";
		end if;
	end process p_ledLogic;

end Behavioral;
