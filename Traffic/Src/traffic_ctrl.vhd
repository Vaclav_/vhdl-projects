----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 28.04.2013 13:46:12
-- Project Name: Traffic
-- Module Name: traffic_ctrl - Structural
-- Description: ez a kesz keresztezodes vezerlo egyseg
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity traffic_ctrl is
    Port (
		i_trainL : in STD_LOGIC;
		i_trainR : in STD_LOGIC;
		i_ctrlON : in STD_LOGIC;
		i_en : in STD_LOGIC;
		i_test : in STD_LOGIC;
		i_clk : in STD_LOGIC;
		i_rst : in STD_LOGIC;
		
		o_a1 : out STD_LOGIC;
		o_b1 : out STD_LOGIC_VECTOR(2 downto 0);
		o_c1 : out STD_LOGIC_VECTOR(2 downto 0);
		o_d1 : out STD_LOGIC_VECTOR(1 downto 0);
		o_a2 : out STD_LOGIC_VECTOR(2 downto 0);
		o_d2 : out STD_LOGIC_VECTOR(1 downto 0);
		o_a3 : out STD_LOGIC_VECTOR(2 downto 0);
		o_a4 : out STD_LOGIC;
		o_b4 : out STD_LOGIC_VECTOR(2 downto 0);
		o_trainLedL : out STD_LOGIC;
		o_trainLedR : out STD_LOGIC;
		o_trainWhite : out STD_LOGIC
		);
end traffic_ctrl;

architecture Structural of traffic_ctrl is


	component NextStateLogic
		Port (
			i_ctrlON : in STD_LOGIC;
			i_curLedState : in STD_LOGIC_VECTOR (7 downto 0);
			o_nextLedState : out STD_LOGIC_VECTOR (7 downto 0)
		);
	end component NextStateLogic;

	component StateMemory
	    Port (
	    	i_nextLedState : in STD_LOGIC_VECTOR (7 downto 0);
			i_en : in STD_LOGIC;
			i_rst : in STD_LOGIC;
			i_clk : in STD_LOGIC;
			o_curLedState : out STD_LOGIC_VECTOR (7 downto 0)
		);
	end component StateMemory;
	
	component TrainState
	    Port (
	    	i_trainL : in STD_LOGIC;
			i_trainR : in STD_LOGIC;
			i_en : in STD_LOGIC;
			i_rst : in STD_LOGIC;
			o_curTrainState : out STD_LOGIC_VECTOR(1 downto 0)
		);
	end component TrainState;
	
	component OutputLogic
	    Port (
	    	i_curTrainState : in STD_LOGIC_VECTOR(1 downto 0);
	    	i_curLedState : in STD_LOGIC_VECTOR(7 downto 0);
	    	i_test : in STD_LOGIC;
	    	i_rst : in STD_LOGIC;
	    	i_clk : in STD_LOGIC;
	    	i_en : in STD_LOGIC;
	    	
	    	o_a1 : out STD_LOGIC;
	    	o_b1 : out STD_LOGIC_VECTOR(2 downto 0);
	    	o_c1 : out STD_LOGIC_VECTOR(2 downto 0);
	    	o_d1 : out STD_LOGIC_VECTOR(1 downto 0);
	    	o_a2 : out STD_LOGIC_VECTOR(2 downto 0);
			o_d2 : out STD_LOGIC_VECTOR(1 downto 0);
			o_a3 : out STD_LOGIC_VECTOR(2 downto 0);
			o_a4 : out STD_LOGIC;
			o_b4 : out STD_LOGIC_VECTOR(2 downto 0);
			
			o_trainLedL : out STD_LOGIC;
			o_trainLedR : out STD_LOGIC;
			o_trainWhite : out STD_LOGIC
	    );
	end component OutputLogic;

	signal s_curLedState : STD_LOGIC_VECTOR(7 downto 0);
	signal s_curTrainState : STD_LOGIC_VECTOR(1 downto 0);
	
	signal s_nextLedState : STD_LOGIC_VECTOR(7 downto 0);

begin

	inst_nextState : NextStateLogic
	port map(
		i_ctrlON => i_ctrlON,
		i_curLedState => s_curLedState,
		o_nextLedState => s_nextLedState
	);
	
	inst_stateMemory : StateMemory
	port map(
		i_nextLedState => s_nextLedState,
		i_en => i_en,
		i_rst => i_rst,
		i_clk => i_clk,
		o_curLedState => s_curLedState
	);
	
	inst_trainState : TrainState
	port map(
		i_trainL => i_trainL,
		i_trainR => i_trainR,
		i_en => i_en,
		i_rst => i_rst,
		o_curTrainState => s_curTrainState
	);
	
	inst_outputLogic : OutputLogic
	port map(
	   	i_curTrainState => s_curTrainState,
		i_curLedState => s_curLedState,
		i_test => i_test,
		i_rst => i_rst,
		i_clk => i_clk,
		i_en => i_en,
	
		o_a1 => o_a1,
		o_b1 => o_b1,
		o_c1 => o_c1,
		o_d1 => o_d1,
		o_a2 => o_a2,
		o_d2 => o_d2,
		o_a3 => o_a3,
		o_a4 => o_a4,
		o_b4 => o_b4,
			
		o_trainLedL => o_trainLedL,
		o_trainLedR => o_trainLedR,
		o_trainWhite => o_trainWhite
	);
	
	

end Structural;
