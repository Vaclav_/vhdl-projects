----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 28.04.2013 15:30:41
-- Project Name: Traffic
-- Module Name: traffic_ctrl_top - Structural
-- Description:
--		ez a modul lesz implementalva az FPGAba
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity traffic_ctrl_top is
    Port (
		i_trainL : in STD_LOGIC;
		i_trainR : in STD_LOGIC;
		i_ctrlON : in STD_LOGIC;
		i_en : in STD_LOGIC;
		i_test : in STD_LOGIC;
		i_sysClk : in STD_LOGIC;
		i_rst : in STD_LOGIC;
		
		o_a1 : out STD_LOGIC;
		o_b1 : out STD_LOGIC_VECTOR(2 downto 0);
		o_c1 : out STD_LOGIC_VECTOR(2 downto 0);
		o_d1 : out STD_LOGIC_VECTOR(1 downto 0);
		o_a2 : out STD_LOGIC_VECTOR(2 downto 0);
		o_d2 : out STD_LOGIC_VECTOR(1 downto 0);
		o_a3 : out STD_LOGIC_VECTOR(2 downto 0);
		o_a4 : out STD_LOGIC;
		o_b4 : out STD_LOGIC_VECTOR(2 downto 0);
		o_trainLedL : out STD_LOGIC;
		o_trainLedR : out STD_LOGIC;
		o_trainWhite : out STD_LOGIC
    );
    
end traffic_ctrl_top;

architecture Structural of traffic_ctrl_top is

	component clk_div is
	   generic (
				input_freq   : integer := 100_000_000;   -- default value: 100 MHz
				desired_freq : integer := 100            -- default value: 100 Hz
			   );
	   port (
			 clk_in  : in  std_logic;   -- clock input
			 clk_out : out std_logic    -- clock output
			);
	end component clk_div;

	component traffic_ctrl
    Port (
		i_trainL : in STD_LOGIC;
		i_trainR : in STD_LOGIC;
		i_ctrlON : in STD_LOGIC;
		i_en : in STD_LOGIC;
		i_test : in STD_LOGIC;
		i_clk : in STD_LOGIC;
		i_rst : in STD_LOGIC;
		
		o_a1 : out STD_LOGIC;
		o_b1 : out STD_LOGIC_VECTOR(2 downto 0);
		o_c1 : out STD_LOGIC_VECTOR(2 downto 0);
		o_d1 : out STD_LOGIC_VECTOR(1 downto 0);
		o_a2 : out STD_LOGIC_VECTOR(2 downto 0);
		o_d2 : out STD_LOGIC_VECTOR(1 downto 0);
		o_a3 : out STD_LOGIC_VECTOR(2 downto 0);
		o_a4 : out STD_LOGIC;
		o_b4 : out STD_LOGIC_VECTOR(2 downto 0);
		o_trainLedL : out STD_LOGIC;
		o_trainLedR : out STD_LOGIC;
		o_trainWhite : out STD_LOGIC
		);
	end component traffic_ctrl;

	signal s_clkDiv : STD_LOGIC;
	
begin

	inst_clk_div : clk_div
	generic map(
		input_freq => 100_000_000,	--100MHz
		desired_freq => 16
	)
	Port map(
		clk_in => i_sysClk,
		clk_out => s_clkDiv
	);
	
	inst_traffic_ctrl : traffic_ctrl
	port map(
		i_test => i_test,
		i_rst => i_rst,
		i_clk => s_clkDiv,
		i_en => i_en,
		i_ctrlON => i_ctrlON,
		i_trainL => i_trainL,
		i_trainR => i_trainR,
	
		o_a1 => o_a1,
		o_b1 => o_b1,
		o_c1 => o_c1,
		o_d1 => o_d1,
		o_a2 => o_a2,
		o_d2 => o_d2,
		o_a3 => o_a3,
		o_a4 => o_a4,
		o_b4 => o_b4,
			
		o_trainLedL => o_trainLedL,
		o_trainLedR => o_trainLedR,
		o_trainWhite => o_trainWhite
	);

end Structural;
