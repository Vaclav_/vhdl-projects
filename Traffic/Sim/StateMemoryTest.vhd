----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 04/12/2013 03:16:21 PM
-- Project Name: Traffic
-- Module Name: StateMemoryTest - Behavioral
-- Description:
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity StateMemoryTest is

end StateMemoryTest;

architecture Behavioral of StateMemoryTest is

component StateMemory
    Port (
    	i_nextLedState : in STD_LOGIC_VECTOR (7 downto 0);
		i_en : in STD_LOGIC;
		i_rst : in STD_LOGIC;
		i_clk : in STD_LOGIC;
		o_curLedState : out STD_LOGIC_VECTOR (7 downto 0)
	);
end component StateMemory;

	signal t_nextLedState : STD_LOGIC_VECTOR (7 downto 0) := x"10";
	signal t_curLedState : STD_LOGIC_VECTOR (7 downto 0);
	signal t_en : STD_LOGIC := '1';
	signal t_rst : STD_LOGIC;
	signal t_clk : STD_LOGIC;

begin

	uutStateMemory : StateMemory
	port map(
		i_nextLedState => t_nextLedState,
		o_curLedState => t_curLedState,
		i_en => t_en,
		i_rst => t_rst,
		i_clk => t_clk
	);

	p_clock : process
	begin
		t_clk <= '0'; wait for 75 ns;
		t_clk <= '1'; wait for 75 ns;
	end process p_clock;
	
	p_stimEn : process
	begin
		t_en <=	'1',
				'0' after 525 ns,
				'1' after 800 ns;
		wait;
	end process p_stimEn;

	p_stimRst : process
	begin
		t_rst <=	'0',
					'1' after 500 ns,
					'0' after 550 ns;
		wait;
	end process p_stimRst;

end Behavioral;
