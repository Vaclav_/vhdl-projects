----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 12.04.2013 23:55:32
-- Project Name: Traffic
-- Module Name: NextStateLogicTest - Behavioral
-- Description: szimulacios ido: 2000 ns
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity NextStateLogicTest is
end NextStateLogicTest;

architecture Behavioral of NextStateLogicTest is

component NextStateLogic
    Port (
		i_ctrlON : in STD_LOGIC;
		i_curLedState : in STD_LOGIC_VECTOR (7 downto 0);
		o_nextLedState : out STD_LOGIC_VECTOR (7 downto 0)
	);
end component NextStateLogic;

component StateMemory
    Port (
    	i_nextLedState : in STD_LOGIC_VECTOR (7 downto 0);
		i_en : in STD_LOGIC;
		i_rst : in STD_LOGIC;
		i_clk : in STD_LOGIC;
		o_curLedState : out STD_LOGIC_VECTOR (7 downto 0)
	);
end component StateMemory;

	signal t_nextLedState : STD_LOGIC_VECTOR (7 downto 0) := x"10";
	signal t_curLedState : STD_LOGIC_VECTOR (7 downto 0);
	signal t_en : STD_LOGIC := '1';
	signal t_rst : STD_LOGIC;
	signal t_clk : STD_LOGIC;

	signal t_ctrlON : STD_LOGIC;

begin

	uut0 : StateMemory
	port map(
		i_nextLedState => t_nextLedState,
		o_curLedState => t_curLedState,
		i_en => t_en,
		i_rst => t_rst,
		i_clk => t_clk
	);
	
	uut1 : NextStateLogic
	port map(
		i_ctrlON => t_ctrlON,
		i_curLedState => t_curLedState,
		o_nextLedState => t_nextLedState
	);
	
	p_stimEn : process
	begin
		t_en <=	'1',
				'0' after 1100 ns,
				'1' after 1200 ns;
		wait;
	end process p_stimEn;

	p_stimRst : process
	begin
		t_rst <=	'0',
					'1' after 1300 ns,
					'0' after 1400 ns;
		wait;
	end process p_stimRst;
	
	p_clock : process
	begin
		t_clk <= '0'; wait for 2 ns;
		t_clk <= '1'; wait for 2 ns;
	end process p_clock;
	
	p_onOff : process
	begin
		t_ctrlON <=	'0',
					'1' after 50 ns,
					'0' after 1500 ns,
					'1' after 1800 ns;
		wait;
	end process p_onOff;
	
end Behavioral;
