----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 28.04.2013 14:06:20
-- Project Name: Traffic
-- Module Name: traffic_ctrl_testbench - Behavioral 
-- Description:
--		szimulacio menete:
--		idotartam: 1000 ns
--		0-50 ns teszt funkcio
--		50-100 ns reset
--		100-800 ns bekapcsolva, 500-550 ns enable
--		800-900 ns vonat
--		900-1000 ns kikapcsolás
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity traffic_ctrl_testbench is
end traffic_ctrl_testbench;

architecture Behavioral of traffic_ctrl_testbench is

	component traffic_ctrl
		Port (
			i_trainL : in STD_LOGIC;
			i_trainR : in STD_LOGIC;
			i_ctrlON : in STD_LOGIC;
			i_en : in STD_LOGIC;
			i_test : in STD_LOGIC;
			i_clk : in STD_LOGIC;
			i_rst : in STD_LOGIC;
			
			o_a1 : out STD_LOGIC;
			o_b1 : out STD_LOGIC_VECTOR(2 downto 0);
			o_c1 : out STD_LOGIC_VECTOR(2 downto 0);
			o_d1 : out STD_LOGIC_VECTOR(1 downto 0);
			o_a2 : out STD_LOGIC_VECTOR(2 downto 0);
			o_d2 : out STD_LOGIC_VECTOR(1 downto 0);
			o_a3 : out STD_LOGIC_VECTOR(2 downto 0);
			o_a4 : out STD_LOGIC;
			o_b4 : out STD_LOGIC_VECTOR(2 downto 0);
			o_trainLedL : out STD_LOGIC;
			o_trainLedR : out STD_LOGIC;
			o_trainWhite : out STD_LOGIC
			);
	end component traffic_ctrl;
	
	signal t_trainL : STD_LOGIC;
	signal t_trainR : STD_LOGIC;
	signal t_ctrlON : STD_LOGIC;
	signal t_en : STD_LOGIC;
	signal t_test : STD_LOGIC;
	signal t_clk : STD_LOGIC;
	signal t_rst : STD_LOGIC;

	signal t_a1 : STD_LOGIC;
	signal t_b1 : STD_LOGIC_VECTOR(2 downto 0);
	signal t_c1 : STD_LOGIC_VECTOR(2 downto 0);
	signal t_d1 : STD_LOGIC_VECTOR(1 downto 0);
	signal t_a2 : STD_LOGIC_VECTOR(2 downto 0);
	signal t_d2 : STD_LOGIC_VECTOR(1 downto 0);
	signal t_a3 : STD_LOGIC_VECTOR(2 downto 0);
	signal t_a4 : STD_LOGIC;
	signal t_b4 : STD_LOGIC_VECTOR(2 downto 0);
	signal t_trainLedL : STD_LOGIC;
	signal t_trainLedR : STD_LOGIC;
	signal t_trainWhite : STD_LOGIC;

begin

	inst_traffic_ctrl : traffic_ctrl
	port map(
		i_trainL => t_trainL,
		i_trainR => t_trainR,
		i_ctrlON => t_ctrlON,
		i_en => t_en,
		i_test => t_test,
		i_clk => t_clk,
		i_rst => t_rst,
		
		o_a1 => t_a1,
		o_b1 => t_b1,
		o_c1 => t_c1,
		o_d1 => t_d1,
		o_a2 => t_a2,
		o_d2 => t_d2,
		o_a3 => t_a3,
		o_a4 => t_a4,
		o_b4 => t_b4,
		o_trainLedL => t_trainLedL,
		o_trainLedR => t_trainLedR,
		o_trainWhite => t_trainWhite
	);
	
	p_clock : process
	begin
		t_clk <= '0';
		wait for 2 ns;
		t_clk <= '1';
		wait for 2 ns;
	end process p_clock;
	
	p_stim : process
	begin
		t_en <= '1',
				'0' after 500 ns,
				'1' after 550 ns;

		t_test <= 	'0',
					'1' after 25 ns,
					'0' after 50 ns;

		t_rst <=	'0',
					'1' after 60 ns,
					'0' after 100 ns;

		t_ctrlON <= '0',
					'1' after 70 ns,
					'0' after 905 ns;

		t_trainL <=	'0',
					'1' after 810 ns,
					'0' after 870 ns;
					
		t_trainR <= '0',
					'1' after 830 ns,
					'0' after 900 ns;
		wait;

	end process p_stim;
	
end Behavioral;
