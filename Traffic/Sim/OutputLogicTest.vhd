----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 04/22/2013 12:10:43 PM
-- Project Name: Traffic
-- Module Name: OutputLogicTest - Behavioral
-- Description:
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity OutputLogicTest is
end OutputLogicTest;

architecture Behavioral of OutputLogicTest is

component OutputLogic
    Port (
    	i_curTrainState : in STD_LOGIC_VECTOR(1 downto 0);
    	i_curLedState : in STD_LOGIC_VECTOR(7 downto 0);
    	i_test : in STD_LOGIC;
    	i_rst : in STD_LOGIC;
    	i_clk : in STD_LOGIC;
    	i_en : in STD_LOGIC;
    	
    	o_a1 : out STD_LOGIC;
    	o_b1 : out STD_LOGIC_VECTOR(2 downto 0);
    	o_c1 : out STD_LOGIC_VECTOR(2 downto 0);
    	o_d1 : out STD_LOGIC_VECTOR(1 downto 0);
    	o_a2 : out STD_LOGIC_VECTOR(2 downto 0);
		o_d2 : out STD_LOGIC_VECTOR(1 downto 0);
		o_a3 : out STD_LOGIC_VECTOR(2 downto 0);
		o_a4 : out STD_LOGIC;
		o_b4 : out STD_LOGIC_VECTOR(2 downto 0);
		
		o_trainLedL : out STD_LOGIC;
		o_trainLedR : out STD_LOGIC;
		o_trainWhite : out STD_LOGIC
    );
end component OutputLogic;

	signal t_curTrainState : STD_LOGIC_VECTOR(1 downto 0) := b"00";
	signal t_curLedState : STD_LOGIC_VECTOR(7 downto 0) := x"ba";
	signal t_test : STD_LOGIC := '0';
	signal t_rst : STD_LOGIC := '0';
	signal t_clk : STD_LOGIC;
	signal t_en : STD_LOGIC := '1';
	
	signal t_a1 : STD_LOGIC;
	signal t_b1 : STD_LOGIC_VECTOR(2 downto 0);
	signal t_c1 : STD_LOGIC_VECTOR(2 downto 0);
	signal t_d1 : STD_LOGIC_VECTOR(1 downto 0);
	signal t_a2 : STD_LOGIC_VECTOR(2 downto 0);
	signal t_d2 : STD_LOGIC_VECTOR(1 downto 0);
	signal t_a3 : STD_LOGIC_VECTOR(2 downto 0);
	signal t_a4 : STD_LOGIC;
	signal t_b4 : STD_LOGIC_VECTOR(2 downto 0);
	
	signal t_trainLedL : STD_LOGIC;
	signal t_trainLedR : STD_LOGIC;
	signal t_trainWhite : STD_LOGIC;

begin

	uut : OutputLogic
	port map(
		i_curTrainState => t_curTrainState,
		i_curLedState => t_curLedState,
		i_test => t_test,
		i_rst => t_rst,
		i_clk => t_clk,
		i_en => t_en,
		o_a1 => t_a1,
		o_b1 => t_b1,
		o_c1 => t_c1,
		o_d1 => t_d1,
		o_a2 => t_a2,
		o_d2 => t_d2,
		o_a3 => t_a3,
		o_a4 => t_a4,
		o_b4 => t_b4,
		o_trainLedL => t_trainLedL,
		o_trainLedR => t_trainLedR,
		o_trainWhite => t_trainWhite
	);
	
	p_clock : process
	begin
		t_clk <= '0'; wait for 25 ns;
		t_clk <= '1'; wait for 25 ns;
	end process p_clock;
	
	p_rst : process
	begin
		t_rst <= '0';
		wait;
	end process p_rst;
	
	p_Train : process
	begin
		t_curTrainState <= b"00",
							b"01" after 300 ns,
							b"11" after 600 ns,
							b"00" after 660 ns;
		wait;
	end process p_Train;
	
	p_ledState : process
	begin
		t_curLedState <= x"00",
						x"0c" after 200 ns,
						x"0d" after 400 ns,
						x"0e" after 600 ns,
						x"0f" after 800 ns;
		wait;
	end process p_ledState;

end Behavioral;
