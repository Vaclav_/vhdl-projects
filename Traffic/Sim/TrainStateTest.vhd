----------------------------------------------------------------------------------
-- Company: Obuda University Kando Kalman Faculty of Electrical Engineering MAI F4
-- Engineer: Patrik Turi
-- Create Date: 13.04.2013 12:36:12
-- Project Name: Traffic
-- Module Name: StateMemoryTest - Behavioral
-- Description:
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TrainStateTest is
end TrainStateTest;

architecture Behavioral of TrainStateTest is

	component TrainState
		Port (
			i_trainL : in STD_LOGIC;
			i_trainR : in STD_LOGIC;
			i_en : in STD_LOGIC;
			i_rst : in STD_LOGIC;
			o_curTrainState : out STD_LOGIC_VECTOR(1 downto 0)
		);
	end component TrainState;

	signal t_trainL : STD_LOGIC;
	signal t_trainR : STD_LOGIC;
	signal t_en : STD_LOGIC;
	signal t_rst : STD_LOGIC;
	signal t_curTrainState : STD_LOGIC_VECTOR(1 downto 0);
	
begin

	uut : TrainState
	port map(
		i_trainL => t_trainL,
		i_trainR => t_trainR,
		i_en => t_en,
		i_rst => t_rst,
		o_curTrainState => t_curTrainState
	);

	p_stimL : process
	begin
		t_trainR <= 	'0',
						'1' after 300 ns,
						'0' after 600 ns,
						'1' after 700 ns,
						'0' after 800 ns;
		wait;
	end process p_stimL;
	
	p_stimR : process
	begin
		t_trainL <= 	'0',
						'1' after 200 ns,
						'0' after 400 ns;
		wait;
	end process p_stimR;

	p_stimEn : process
	begin
		t_en <= '0',
				'1' after 650 ns;
		wait;
	end process p_stimEn;

	p_stimRst : process
	begin
		t_rst <= 	'0',
					'1' after 400 ns,
					'0' after 550 ns;
		wait;
	end process p_stimRst;

end Behavioral;
